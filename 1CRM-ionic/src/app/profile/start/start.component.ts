import { Component, OnInit } from "@angular/core";
import { AppService } from "src/controller/apiAppData/app.service";
import { ProfileMainService } from "src/controller/services/profileServ/profile-main.service";
import { CountryISO, SearchCountryField } from "ngx-intl-tel-input";
import { NavigationService } from "src/controller/services/navigationRoute/navigation.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import Collection from "src/controller/utils/Collection";

@Component({
  selector: "app-start",
  templateUrl: "./start.component.html",
  styleUrls: ["./start.component.scss"],
})
export class StartComponent implements OnInit {
  customActionSheetOptions: any = {
    header: "Choose one",
  };

  registerForm: FormGroup;
  items: any
  allCountry: Array<any> = [];
  SearchCountryField = SearchCountryField;
  telephonePlaceholder = {
    tel: "Telephone (mobile)* ",
    alt: "Telephone (alternative)",
  };
  CountryISO = CountryISO;
  id: any;
  tabChk = 1;
  address2Check = true;
  checkDLvalue: any;
  constructor(
    private app: AppService,
    private navCtrl: NavigationService,
    private fb: FormBuilder,
    private profileService: ProfileMainService
  ) {
    this.id = JSON.parse(localStorage.getItem("user_profile"));
    this.allCountry = Object.keys(this.CountryISO);
    this.items = Collection.dlCheckBoxes;
    this.formData();
  }

  ngOnInit() {
    this.app.getApprovals(this.id.freelancer_id);
    this.profileService.getAllInclude(this.id.freelancer_id).then((user: any)=>{
      if(typeof user !== undefined){
        this.getUserData(user.data)
      }
    });
  }

  formData() {
    this.registerForm = this.fb.group({
      title: ["", []],
      firstname: ["", Validators.required],
      lastname: ["", Validators.required],
      birthdate: ["", Validators.required],
      gender: ["", Validators.required],
      birthcountry: ["", Validators.required],
      country: ["", Validators.required],
      birthplace: ["", []],
      driver_license: ["A", []],
      has_driverslicense: ["true", []],
      mobile_contry_code: ["", []],
      mobile_contry_code2: ["", []],
      mobile: ["", []],
      mobileObj: ["", []],
      alternative_phone: ["", []],
      phoneObj: ["", []],
      email: [this.id.email, []],
      address: ["", Validators.required],
      check: [false, []],
      locality: ["", Validators.required],
      zip: ["", Validators.required],
      city: ["", Validators.required],
      near_to_city: ["", []],
      address2: ["", []],
      locality_alternative: ["", []],
      zip2: ["", []],
      city2: ["", []],
      country2: ["", []],
      near_to_city2: ["", []],
    });
  }

  checkValue(e: any){
    this.address2Check = this.registerForm.value.check
  }

  async save() {
    this.registerForm.patchValue({
      mobile: this.registerForm.value.mobileObj.number,
      mobile_contry_code: this.registerForm.value.mobileObj.countryCode,
    });
    this.registerForm.patchValue({
      alternative_phone: this.registerForm.value.phoneObj.number,
      mobile_contry_code2: this.registerForm.value.phoneObj.countryCode,
    });
    this.registerForm.patchValue({has_driverslicense: Boolean(this.registerForm.value.has_driverslicense)});
    if(this.registerForm.value.driver_license){
      this.registerForm.patchValue({driver_license: this.registerForm.value.driver_license.join(",")});
    }
    const data = { state: "saved" };
    this.registerForm.markAllAsTouched();
    if (!this.registerForm.invalid) {
      this.app.freeLancers(this.registerForm.value, this.id.freelancer_id).then((res: any) => {
        if(res){
          this.tabChk = 2;
          this.registerForm.reset();
        }
      })
      .catch((error) => {
        console.log("error", error);
      });
      this.app.freeLancersApproval(data, this.id.freelancer_id);
    }
  }

  getUserData(data){
    console.log(data);
    this.registerForm.get('title').setValue(data.title);
    this.registerForm.get('firstname').setValue(data.firstname);
    this.registerForm.get('lastname').setValue(data.lastname);
    this.registerForm.get('birthdate').setValue(data.birthdate);
    this.registerForm.get('gender').setValue(data.gender);
    this.registerForm.get('birthcountry').setValue(data.birthcountry);
    this.registerForm.get('has_driverslicense').setValue(data.has_driverslicense);
    this.registerForm.get('driver_license').setValue(data.driver_license);
    this.registerForm.get('country').setValue(data.country);
    this.registerForm.get('birthplace').setValue(data.birthplace);
    this.registerForm.get('mobileObj').setValue(data.mobile);
    this.registerForm.get('phoneObj').setValue(data.alternative_phone);
    this.registerForm.get('address').setValue(data.address);
    this.registerForm.get('locality').setValue(data.locality);
    this.registerForm.get('zip').setValue(data.zip);
    this.registerForm.get('city').setValue(data.city);
    this.registerForm.get('near_to_city').setValue(data.near_to_city);
    this.registerForm.get('address2').setValue(data.address2);
    this.registerForm.get('locality_alternative').setValue(data.locality_alternative);
    this.registerForm.get('zip2').setValue(data.zip2);
    this.registerForm.get('city2').setValue(data.city2);
    this.registerForm.get('country2').setValue(data.country2);
    this.registerForm.get('near_to_city2').setValue(data.near_to_city2);
  }

  back() {
    this.navCtrl.goBack();
  }

  dlCheck(e){
    this.checkDLvalue = e.target.value
  }

  openTab(e: any) {
    this.tabChk = e;
  }
}
