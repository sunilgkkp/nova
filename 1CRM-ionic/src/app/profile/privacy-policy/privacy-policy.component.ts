import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss'],
})
export class PrivacyPolicyComponent implements OnInit {

  constructor(public navCtrl: NavigationService) { }

  ngOnInit() { }

  decline() {
    this.navCtrl.goTo('auth/register');
  }
  accept() {
    this.navCtrl.goTo('/auth/select-language');
  }

}
