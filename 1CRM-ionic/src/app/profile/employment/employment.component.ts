import { AddSkillsPage } from './../popups/add-skills/add-skills.page';
import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';
import { AddEmployeePage } from '../popups/add-employee/add-employee.page';
import { AddRolePage } from '../popups/add-role/add-role.page';
import { ReferencesPage } from '../popups/references/references.page';

@Component({
  selector: 'app-employment',
  templateUrl: './employment.component.html',
  styleUrls: ['./employment.component.scss'],
})
export class EmploymentComponent implements OnInit {

  customActionSheetOptions: any = {
    header: 'Choose one',
  };

  industries = [0];
  increseIndu = 1;
  constructor(public navCtrl: NavigationService) { }

  ngOnInit() { }

  back() {
    this.navCtrl.goBack();
  }

  saveNext() {
    this.navCtrl.goTo('/profile/legal');
  }

  addEmployee() {
    this.navCtrl.popup(AddEmployeePage)
  }

  addRoles() {
    this.navCtrl.popup(AddRolePage)
  }

  addIndustry() {
    this.industries.push(this.increseIndu++)
  }

  delete(i) {
    this.industries.splice(i, 1);
  }

  addSkill() {
    this.navCtrl.popup(AddSkillsPage);
  }

  addReference() {
    this.navCtrl.popup(ReferencesPage)
  }
}
