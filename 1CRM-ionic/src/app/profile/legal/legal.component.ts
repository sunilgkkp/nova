import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CountryISO } from 'ngx-intl-tel-input';
import { FunctionService } from 'src/controller/services/function/function.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';
import { AddIdentityPage } from '../popups/add-identity/add-identity.page';

@Component({
  selector: 'app-legal',
  templateUrl: './legal.component.html',
  styleUrls: ['./legal.component.scss'],
})
export class LegalComponent implements OnInit {

  customActionSheetOptions: any = {
    header: 'Choose one',
  };

  registerForm: FormGroup;
  CountryISO = CountryISO;
  allCountry: Array<any> = []
  constructor(
    public navCtrl: NavigationService,
    public fun: FunctionService,
    private fb: FormBuilder,
  ) {
    this.allCountry = Object.keys(this.CountryISO);
  }

  ngOnInit() {
    this.formData()
   }

  formData(){
    this.registerForm = this.fb.group({
      account: ['', Validators.required],
      bname: ['', Validators.required],
      iban: ['', Validators.required],
      bic: ['', []],
      country: ['', []],
      radio: ['', []],
      radio1: ['', []],
    });
  }

  back() {
    this.navCtrl.goBack();
  }

  imagesNew() {
    this.fun.presentActionSheet();
  }

  addIdentity() {
    this.navCtrl.popup(AddIdentityPage)
  }

  save(){
    this.registerForm.markAllAsTouched();
    if (this.registerForm.valid) {
     console.log('Data... ', this.registerForm.value);
     this.registerForm.reset();
    }
  }

}
