import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { FunctionService } from 'src/controller/services/function/function.service';
import { GeneralService } from 'src/controller/services/general/general.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';
import Collection from 'src/controller/utils/Collection';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss'],
})
export class MainMenuComponent implements OnInit {

  menuItem: any;
  constructor(
    private navCtrl: NavigationService,
    private generalService: GeneralService,
    private auth: AuthenticationService,
    public fun: FunctionService
  ) {
    this.menuItem = Collection.menuData;
  }

  ngOnInit() { }

  goto(url) {
    this.navCtrl.goTo(url);
    this.fun.closeMenu();
  }

  logout() {
    this.generalService.confirmAlert({
      header: 'auth.logout.confirm',
      cancel: true,
      confirm: true,
      onConfirm: () => {
        this.auth.logout()
      },
    });
  }


}
