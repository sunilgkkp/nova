import { LegalComponent } from './legal/legal.component';
import { EmploymentComponent } from './employment/employment.component';
import { AppearanceComponent } from './appearance/appearance.component';
import { StartComponent } from './start/start.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxIntlTelInputModule } from "ngx-intl-tel-input";
import { IonicModule } from '@ionic/angular';

import { ProfilePageRoutingModule } from './profile-routing.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { ProfilePage } from './profile.page';
import { QualificationsComponent } from './qualifications/qualifications.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxIntlTelInputModule,
    AngularEditorModule,
    ReactiveFormsModule,
    ProfilePageRoutingModule
  ],
  declarations: [
    ProfilePage,
    StartComponent,
    AppearanceComponent,
    QualificationsComponent,
    EmploymentComponent,
    LegalComponent,
  ]
})
export class ProfilePageModule { }
