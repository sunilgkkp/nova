import { Component, OnInit, ElementRef } from '@angular/core';
import { FunctionService } from 'src/controller/services/function/function.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';

@Component({
  selector: 'app-appearance',
  templateUrl: './appearance.component.html',
  styleUrls: ['./appearance.component.scss'],
})
export class AppearanceComponent implements OnInit {

  constructor(
    private navCtrl: NavigationService,
    private ele: ElementRef,
    public fun: FunctionService
  ) { }

  ngOnInit() { }

  back() {
    this.navCtrl.goBack();
  }

  imgUpload() {
    const el = this.ele.nativeElement.querySelector('.d-label-block')
    el.classList.toggle('d-label')
  }

  newImages() {
    this.fun.presentActionSheet();
  }

  uploadLoad() {
    const el = this.ele.nativeElement.querySelector('.d-label-1')
    el.classList.toggle('d-label')
  }

  imagesNew() {
    this.fun.presentActionSheet();
  }

  saveNext() {
    // this.navCtrl.goTo('/profile/qualifications');
  }

}
