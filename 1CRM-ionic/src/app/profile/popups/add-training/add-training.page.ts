import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from 'src/controller/apiAppData/app.service';
import { FunctionService } from 'src/controller/services/function/function.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';

@Component({
  selector: 'app-add-training',
  templateUrl: './add-training.page.html',
  styleUrls: ['./add-training.page.scss'],
})
export class AddTrainingPage implements OnInit {

  trainings = [0];
  increseTrainee = 1;
  registerForm: FormGroup;
  constructor(
    public navCtrl: NavigationService,
    public fun: FunctionService,
    public fb: FormBuilder,
    public app: AppService
  ) { }

  ngOnInit() {
    this.formData()
  }

  formData(){
    this.registerForm = this.fb.group({
      name: ['', []],
      issuing_organization: ['', []],
      issue_date: ['', []],
      description: ['', []],
    });
  }

  back() {
    this.navCtrl.modalDissmis();
  }

  imagesNew() {
    this.fun.presentActionSheet();
  }

  moreTraining() {
    this.trainings.push(this.increseTrainee++)
  }
  deleteTraining(i: any) {
    if (i !== 0) {
      this.trainings.splice(i, 1)
    }
    else if (i === 0) {
      this.navCtrl.modalDissmis();
      this.trainings.splice(i, 1)
    }
  }

  submit(){
    this.registerForm.markAllAsTouched();
    if (this.registerForm.valid) {
      this.registerForm.value.issue_date = this.fun.transformDate(this.registerForm.value.issue_date, 'y-MM-dd');
      this.app.addTraining(this.registerForm.value).then((res: any) => {
        if(res){
          this.navCtrl.modalDissmis();
          console.log('Data... ', this.registerForm.value);
          this.registerForm.reset();
        }
      })
    }
  }
}
