import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppService } from 'src/controller/apiAppData/app.service';
import { FunctionService } from 'src/controller/services/function/function.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.page.html',
  styleUrls: ['./add-employee.page.scss'],
})
export class AddEmployeePage implements OnInit {

  employees = [0];
  registerForm: FormGroup;
  useData: any;
  increaseEmp = 1;
  constructor(
    public navCtrl: NavigationService,
    public fun: FunctionService,
    public fb: FormBuilder,
    public app: AppService
  ) { 
    let getuser = localStorage.getItem('user_profile');
    this.useData = JSON.parse(getuser);
  }

  ngOnInit() {
    this.formData();
  }

  formData() {
    this.registerForm = this.fb.group({
      job_title: ['', []],
      company_name: ['', []],
      is_current_company: ['', []],
      start_from: ['', []],
      till: ['', []],
      freelancer_id: ['', []],
      year: ['', []],
      month: ['', []],
      description: ['', []],
    });
  }


  back() {
    this.navCtrl.modalDissmis();
  }

  imagesNew() {
    this.fun.presentActionSheet();
  }

  addMoreEmployee() {
    this.employees.push(this.increaseEmp++)
  }
  deleteEmployee(i: any) {
    if (i !== 0) {
      this.employees.splice(i, 1);
    }
    else if (i == 0) {
      this.navCtrl.modalDissmis();
      this.employees.splice(i, 1);
    }
  }

  save(){
    this.registerForm.markAllAsTouched();
    this.registerForm.patchValue({freelancer_id: this.useData.freelancer_id})
    if(this.registerForm.value.is_current_company == 'true'){
      this.registerForm.patchValue({is_current_company: true})
    }
    else{
      this.registerForm.patchValue({is_current_company: false})
    }
    if (this.registerForm.valid) {
      this.registerForm.value.start_from = this.fun.transformDate(this.registerForm.value.start_from, 'y-MM-dd');
      this.registerForm.value.till = this.fun.transformDate(this.registerForm.value.till, 'y-MM-dd');
      this.registerForm.value.year = this.fun.transformDate(this.registerForm.value.year, 'y-MM-dd');
      this.registerForm.value.month = this.fun.transformDate(this.registerForm.value.month, 'y-MM-dd');
      this.app.addEmployee(this.registerForm.value).then((res: any) => {
        console.log('res =', res);
        if(res){
          console.log('Data... ', this.registerForm.value);
          this.navCtrl.modalDissmis();
          this.registerForm.reset();
        }     
      })
    }
  }
}
