import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';

@Component({
  selector: 'app-add-role',
  templateUrl: './add-role.page.html',
  styleUrls: ['./add-role.page.scss'],
})
export class AddRolePage implements OnInit {
  customActionSheetOptions: any = {
    header: 'Choose one',
  };

  roles = [0]
  increseRole = 1;
  registerForm: FormGroup
  constructor(
    public navCtrl: NavigationService,
    public fb: FormBuilder
  ) { }

  ngOnInit() {
    this.formData();
  }

  back() {
    this.navCtrl.modalDissmis();
  }

  addRoles() {
    this.roles.push(this.increseRole++)
  }

  deleteRole(i: any) {
    if (i !== 0) {
      this.roles.splice(i, 1);
    }
    else if (i === 0) {
      this.navCtrl.modalDissmis();
      this.roles.splice(i, 1);
    }
  }

  formData(){
    this.registerForm = this.fb.group({
      primary_role: ['', []],
      role: ['', []],
      role_des: ['', []],
    });
  }

  save(){
    this.registerForm.markAllAsTouched();
    if (this.registerForm.valid) {
     console.log('Data... ', this.registerForm.value);
     this.registerForm.reset();
    }
  }

}
