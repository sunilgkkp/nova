import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddIdentityPageRoutingModule } from './add-identity-routing.module';

import { AddIdentityPage } from './add-identity.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    AddIdentityPageRoutingModule
  ],
  declarations: [AddIdentityPage]
})
export class AddIdentityPageModule {}
