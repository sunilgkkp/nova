import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddIdentityPage } from './add-identity.page';

const routes: Routes = [
  {
    path: '',
    component: AddIdentityPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddIdentityPageRoutingModule {}
