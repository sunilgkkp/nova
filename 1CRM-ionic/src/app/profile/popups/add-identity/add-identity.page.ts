import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FunctionService } from 'src/controller/services/function/function.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';

@Component({
  selector: 'app-add-identity',
  templateUrl: './add-identity.page.html',
  styleUrls: ['./add-identity.page.scss'],
})
export class AddIdentityPage implements OnInit {

  identities = [0];
  increaseIdentity = 1;
  registerForm: FormGroup
  constructor(
    public navCtrl: NavigationService,
    public fun: FunctionService,
    public fb: FormBuilder
  ) { }

  ngOnInit() {
    this.formData();
  }

  formData() {
    this.registerForm = this.fb.group({
      id_name: ['', Validators.required],
      id_number: ['', Validators.required],
      exp_date: ['', []],
    });
  }

  back() {
    this.navCtrl.modalDissmis();
  }

  imagesNew() {
    this.fun.presentActionSheet()
  }

  addMoreIdentity() {
    this.identities.push(this.increaseIdentity++)
  }

  delete(i: any) {
    if (i !== 0) {
      this.identities.splice(i, 1);
    }
    else if (i == 0) {
      this.navCtrl.modalDissmis();
      this.identities.splice(i, 1);
    }
  }

  save() {
    this.registerForm.markAllAsTouched();
    if (this.registerForm.valid) {
      console.log('Data... ', this.registerForm.value);
      this.registerForm.reset();
    }
  }

}
