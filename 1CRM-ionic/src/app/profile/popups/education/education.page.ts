import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from 'src/controller/apiAppData/app.service';
import { FunctionService } from 'src/controller/services/function/function.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';

@Component({
  selector: 'app-education',
  templateUrl: './education.page.html',
  styleUrls: ['./education.page.scss'],
})
export class EducationPage implements OnInit {
  customActionSheetOptions: any = {
    header: 'Choose one',
  };

  registerForm: FormGroup;
  qualifications = [0];
  increaseQualification = 1;
  constructor(
    public navCtrl: NavigationService,
    public fun: FunctionService,
    private fb: FormBuilder,
    private app: AppService
  ) { }

  ngOnInit() {
    this.formData();
  }

  formData(){
    this.registerForm = this.fb.group({
      school_college_university: ['', [Validators.required]],
      degree: ['', Validators.required],
      fieldofstudy: ['', []],
      end_date: ['', Validators.required],
      grade: ['', Validators.required],
      start_date: ['', Validators.required],
      description: ['', Validators.required],
    });
  }


  back() {
    this.navCtrl.modalDissmis();
  }

  imagesNew() {
    this.fun.presentActionSheet();
  }

  addQualification() {
    this.qualifications.push(this.increaseQualification++)
  }

  qulificationDelete(i: any) {
    if (i !== 0) {
      this.qualifications.splice(i, 1);
    }
    else if (i == 0) {
      this.navCtrl.modalDissmis();
      this.qualifications.splice(i, 1);
    }
  }

  save(){
    this.registerForm.markAllAsTouched();
    if (this.registerForm.valid) {
      this.registerForm.value.end_date = this.fun.transformDate(this.registerForm.value.end_date, 'y-MM-dd');
      this.registerForm.value.start_date = this.fun.transformDate(this.registerForm.value.start_date, 'y-MM-dd');
      this.app.addEducation(this.registerForm.value).then((res: any)=>{
        console.log('res = ', res);
        if(res){
          this.navCtrl.modalDissmis();
        }
      })
     this.registerForm.reset();
    }
  }

}
