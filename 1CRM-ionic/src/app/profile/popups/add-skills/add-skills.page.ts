import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';

@Component({
  selector: 'app-add-skills',
  templateUrl: './add-skills.page.html',
  styleUrls: ['./add-skills.page.scss'],
})
export class AddSkillsPage implements OnInit {

  customActionSheetOptions: any = {
    header: 'Choose one',
  };

  skills = [0];
  registerForm: FormGroup
  increseSkill = 1;
  constructor(
    public navCtrl: NavigationService,
    public fb: FormBuilder
  ) { }

  ngOnInit() {
    this.formData();
  }

  formData(){
    this.registerForm = this.fb.group({
      skill_cate: ['', []],
      skill_name: ['', []],
    });
  }

  back() {
    this.navCtrl.modalDissmis();
  }

  addSkills() {
    this.skills.push(this.increseSkill++)
  }

  deleteSkill(i: any) {
    if (i !== 0) {
      this.skills.splice(i, 1);
    }
    else if (i === 0) {
      this.navCtrl.modalDissmis();
      this.skills.splice(i, 1);
    }
  }

  save(){
    this.registerForm.markAllAsTouched();
    if (this.registerForm.valid) {
     console.log('Data... ', this.registerForm.value);
     this.registerForm.reset();
    }
  }
}
