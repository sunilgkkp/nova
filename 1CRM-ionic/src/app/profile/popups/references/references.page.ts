import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FunctionService } from 'src/controller/services/function/function.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';
import Collection from 'src/controller/utils/Collection';

@Component({
  selector: 'app-references',
  templateUrl: './references.page.html',
  styleUrls: ['./references.page.scss'],
})
export class ReferencesPage implements OnInit {

  customActionSheetOptions: any = {
    header: 'Choose one',
  };

  referal = [0];
  increaseRefer = 1;
  registerForm: FormGroup
  allReferences: any = [];
  constructor(
    public navCtrl: NavigationService,
    public fun: FunctionService,
    public fb: FormBuilder
  ) { }

  ngOnInit() {
    this.allReferences = Collection.references;
    this.formData();
  }

  formData(){
    this.registerForm = this.fb.group({
      name: ['', [Validators.required]],
      reletionship_role: ['', [Validators.required]],
      email: ['', []],
      number: ['', []],
    });
  }

  back() {
    this.navCtrl.modalDissmis();
  }

  imagesNew() {
    this.fun.presentActionSheet();
  }

  addMoreReferences() {
    this.referal.push(this.increaseRefer++)
  }

  deleteReference(i: any) {
    if (i !== 0) {
      this.referal.splice(i, 1)
    }
    else if (i == 0) {
      this.navCtrl.modalDissmis();
      this.referal.splice(i, 1)
    }
    console.log(this.referal);
  }

  save(){
    this.registerForm.markAllAsTouched();
    if (this.registerForm.valid) {
     console.log('Data... ', this.registerForm.value);
     this.registerForm.reset();
    }
  }
}
