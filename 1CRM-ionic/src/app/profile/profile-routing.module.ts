import { LegalComponent } from './legal/legal.component';
import { EmploymentComponent } from './employment/employment.component';
import { AppearanceComponent } from './appearance/appearance.component';
import { StartComponent } from './start/start.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilePage } from './profile.page';
import { QualificationsComponent } from './qualifications/qualifications.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

const routes: Routes = [
  {
    path: '',
    component: ProfilePage
  },
  {
    path: 'start',
    component: StartComponent,
  },
  {
    path: 'appearance',
    component: AppearanceComponent,
  },
  {
    path: 'qualifications',
    component: QualificationsComponent
  },
  {
    path: 'employment',
    component: EmploymentComponent
  },
  {
    path: 'legal',
    component: LegalComponent
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent
  },
  {
    path: 'education',
    loadChildren: () => import('./popups/education/education.module').then( m => m.EducationPageModule)
  },
  {
    path: 'add-training',
    loadChildren: () => import('./popups/add-training/add-training.module').then( m => m.AddTrainingPageModule)
  },
  {
    path: 'add-employee',
    loadChildren: () => import('./popups/add-employee/add-employee.module').then( m => m.AddEmployeePageModule)
  },
  {
    path: 'add-role',
    loadChildren: () => import('./popups/add-role/add-role.module').then( m => m.AddRolePageModule)
  },
  {
    path: 'add-skills',
    loadChildren: () => import('./popups/add-skills/add-skills.module').then( m => m.AddSkillsPageModule)
  },
  {
    path: 'references',
    loadChildren: () => import('./popups/references/references.module').then( m => m.ReferencesPageModule)
  },
  {
    path: 'add-identity',
    loadChildren: () => import('./popups/add-identity/add-identity.module').then( m => m.AddIdentityPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilePageRoutingModule { }
