import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FunctionService } from 'src/controller/services/function/function.service';
import { AddTrainingPage } from '../popups/add-training/add-training.page';
import Collection from 'src/controller/utils/Collection';
import { EducationPage } from '../popups/education/education.page';

@Component({
  selector: 'app-qualifications',
  templateUrl: './qualifications.component.html',
  styleUrls: ['./qualifications.component.scss'],
})
export class QualificationsComponent implements OnInit {

  languages: any;
  lang = [0];
  increseVal = 1;
  htmlContent = '';
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    translate: 'yes',
    height: 'auto',
    enableToolbar: true,
    showToolbar: true,
    sanitize: true,
    placeholder: 'Enter text here...',
  }

  constructor(
    public navCtrl: NavigationService,
    public fun: FunctionService,
  ) { }

  ngOnInit() {
    this.languages = Collection.languages;
  }


  back() {
    this.navCtrl.goBack();
  }

  imagesNew() {
    this.fun.presentActionSheet();
  }

  saveNext() {
    this.navCtrl.goTo('/profile/employment');
  }

  addEducation() {
    this.navCtrl.popup(EducationPage)
  }

  addTraining() {
    this.navCtrl.popup(AddTrainingPage)
  }


  addNewLanguage() {
    this.lang.push(this.increseVal++)
  }

  delete(i: any) {
    this.lang.splice(i, 1);
  }
}
