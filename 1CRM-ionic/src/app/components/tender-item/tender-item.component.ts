import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tender-item',
  templateUrl: './tender-item.component.html',
  styleUrls: ['./tender-item.component.scss'],
})
export class TenderItemComponent implements OnInit {

  @Input() tender: any;
  constructor() { }

  ngOnInit() { }

}
