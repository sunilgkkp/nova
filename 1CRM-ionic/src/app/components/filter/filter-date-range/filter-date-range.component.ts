import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FilterBase } from '../filter.base';

@Component({
  selector: 'app-filter-date-range',
  templateUrl: './filter-date-range.component.html',
  styleUrls: ['./filter-date-range.component.scss'],
})
export class FilterDateRangeComponent extends FilterBase {

  start: Date;
  end: Date;
  @Input('name') name: string;
  @Input('context') context: string;
  @Input('set') initial: any;
  @Output('init') init: EventEmitter<any> = new EventEmitter();
  @Output('action') action: EventEmitter<any> = new EventEmitter();

  constructor(
    protected translate: TranslateService
  ) {
    super(translate);
  }

  ngOnInit() {
    this.start = this.initial && this.initial.start;
    this.end = this.initial && this.initial.end;
    super.ngOnInit();
  }

  value() {
    return {
      start: this.start,
      end: this.end,
    };
  }

  filtered(): string {
    return (this.start || this.end) && super.filtered([this.start || '...', this.end || '...'].join(' - '));
  }

  clear() {
    this.start = null;
    this.end = null;
    this.set();
  }

}
