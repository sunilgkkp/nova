import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FilterBase } from '../filter.base';
import { Options as options } from '../../../../controller/utils/Options';
import { Collections as collections } from 'src/controller/utils/Collections';

@Component({
  selector: 'app-filter-select',
  templateUrl: './filter-select.component.html',
  styleUrls: ['./filter-select.component.scss'],
})
export class FilterSelectComponent extends FilterBase {

  @Input('options') items: any;
  @Input('none') none: any;
  @Input('name') name: string;
  @Input('context') context: string;
  @Input('set') initial: any;
  @Output('init') init: EventEmitter<any> = new EventEmitter();
  @Output('action') action: EventEmitter<any> = new EventEmitter();

  id: any;

  constructor(
    protected translate: TranslateService
  ) {
    super(translate);
  }

  ngOnInit() {
    this.id = this.initial;
    if (typeof this.items === 'string') {
      this.items = options.list(this.items, this.translate);
    }
    if (this.none && !this.initial) {
      this.id = 0;
    }
    super.ngOnInit();
  }

  value() {
    return this.id;
  }

  filtered(): string {
    return this.id && (collections.find(this.items, { id: this.id }) || {}).name;
  }

  clear() {
    this.id = undefined;
    this.set();
  }
}
