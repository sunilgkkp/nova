import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

interface IFilterInterface {
  clear: () => void;
  filtered: () => string;
}

export interface IFilterBarItems {
  [key: string]: {
    type: string;
    set: any;
    options?: any[] | string;
    none?: boolean;
  };
}

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {

  @Input('context') context: string;
  @Input('filters') filters: IFilterBarItems;
  @Output('on-apply') applying: EventEmitter<any> = new EventEmitter();
  @Output('filter-opened') filterOpened: EventEmitter<any> = new EventEmitter();

  opened: boolean;
  selected: any = {};
  filtered: string;
  items: { [key: string]: IFilterInterface } = {};

  constructor() {
  }

  ngOnInit() {
    Object.keys(this.filters).map((key) => {
      this.selected[key] = this.filters[key].set;
    });
  }

  init(key: string, exposed: IFilterInterface) {
    this.items[key] = exposed;
  }

  set(key: string, value: any) {
    this.selected[key] = value;
  }

  apply() {
    this.checkFiltered();
    this.applying.emit(this.selected);
    this.close();
  }

  checkFiltered() {
    this.filtered = Object.keys(this.items).map((key) => this.items[key].filtered()).filter(Boolean).join(', ');
  }

  clear() {
    Object.keys(this.items).map((key) => {
      this.items[key].clear();
    });
    this.apply();
  }

  open() {
    this.opened = true;
    this.filterOpened.emit(this.opened);
  }

  close() {
    this.opened = false;
    this.filterOpened.emit(this.opened);
  }

}
