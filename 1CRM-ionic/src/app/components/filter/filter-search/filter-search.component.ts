import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FilterBase } from '../filter.base';

@Component({
  selector: 'app-filter-search',
  templateUrl: './filter-search.component.html',
  styleUrls: ['./filter-search.component.scss'],
})
export class FilterSearchComponent extends FilterBase {

  public text: string;
  @Input('name') name: string;
  @Input('context') context: string;
  @Input('set') initial: any;
  @Output('init') init: EventEmitter<any> = new EventEmitter();
  @Output('action') action: EventEmitter<any> = new EventEmitter();

  constructor(protected translate: TranslateService) {
    super(translate);
  }

  ngOnInit() {
    this.text = this.initial || '';
    super.ngOnInit();
  }

  value() {
    return this.text;
  }

  filtered(): string {
    return super.filtered(this.text);
  }

  clear() {
    this.text = '';
    this.set();
  }

}
