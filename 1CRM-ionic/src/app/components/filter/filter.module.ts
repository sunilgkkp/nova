import { FilterComponent } from './filter.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgPipesModule } from 'ngx-pipes';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { FilterSearchComponent } from './filter-search/filter-search.component';
import { FilterRangeComponent } from './filter-range/filter-range.component';
import { FilterDateRangeComponent } from './filter-date-range/filter-date-range.component';
import { FilterSelectComponent } from './filter-select/filter-select.component';
import { FilterButtonsComponent } from './filter-buttons/filter-buttons.component';
import { AppliedFilterComponent } from './applied-filter/applied-filter.component';
import { JobSearchComponent } from './job-search/job-search.component';
import { FilterCheckboxComponent } from './filter-checkbox/filter-checkbox.component';



@NgModule({
  declarations: [
    FilterComponent,
    FilterSearchComponent,
    FilterRangeComponent,
    FilterDateRangeComponent,
    FilterSelectComponent,
    FilterButtonsComponent,
    AppliedFilterComponent,
    JobSearchComponent,
    FilterCheckboxComponent
  ],
  imports: [
    CommonModule,
    NgPipesModule,
    IonicModule,
    FormsModule,
    TranslateModule,
  ],
  exports: [FilterComponent, AppliedFilterComponent]
})
export class FilterModule { }
