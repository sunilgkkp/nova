import { Input, Output, EventEmitter, OnInit, Component, Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export abstract class FilterBase implements OnInit {
    @Input('name') name: string;
    @Input('context') context: string;
    @Input('set') initial: any;
    @Output('init') init: EventEmitter<any> = new EventEmitter();
    @Output('action') action: EventEmitter<any> = new EventEmitter();

    selected: any;

    constructor(protected translate: TranslateService) {
    }

    ngOnInit() {
        this.init.emit({
            clear: this.clear.bind(this),
            filtered: this.filtered.bind(this),
        });
        this.set();
    }

    set() {
        this.selected = this.value();
        this.action.emit(this.selected);
    }

    filtered(value: string) {
        return value && [this.translate.instant(this.context + '.filters.' + this.name + '.short'), value].join(': ');
    }

    clear() {
    }

    value() {
        return {};
    }
}
