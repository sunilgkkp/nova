import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FilterBase } from '../filter.base';

@Component({
  selector: 'app-filter-range',
  templateUrl: './filter-range.component.html',
  styleUrls: ['./filter-range.component.scss'],
})
export class FilterRangeComponent extends FilterBase {

  min: any;
  max: any;
  @Input('name') name: string;
  @Input('context') context: string;
  @Input('set') initial: any;
  @Output('init') init: EventEmitter<any> = new EventEmitter();
  @Output('action') action: EventEmitter<any> = new EventEmitter();

  constructor(protected translate: TranslateService) {
    super(translate);
  }

  ngOnInit() {
    this.min = this.initial && this.initial.min;
    this.max = this.initial && this.initial.max;
    super.ngOnInit();
  }

  value() {
    return {
      min: this.min,
      max: this.max,
    };
  }

  filtered(): string {
    return (this.min || this.max) && super.filtered([this.min || '...', this.max || '...'].join(' - '));
  }

  clear() {
    this.min = '';
    this.max = '';
    this.set();
  }

}
