import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FilterBase } from '../filter.base';
import { Options as options } from '../../../../controller/utils/Options';
import { Collections as collections } from 'src/controller/utils/Collections';

@Component({
  selector: 'app-filter-checkbox',
  templateUrl: './filter-checkbox.component.html',
  styleUrls: ['./filter-checkbox.component.scss'],
})
export class FilterCheckboxComponent extends FilterBase {

  @Input('options') items: any;
  @Input('name') name: string;
  @Input('set') initial: any;
  @Input('context') context: string;
  @Output('init') init: EventEmitter<any> = new EventEmitter();
  @Output('action') action: EventEmitter<any> = new EventEmitter();
  public status: string;

  constructor(protected translate: TranslateService) {
    super(translate);
  }

  ngOnInit() {
    if (typeof this.items === 'string') {
      this.items = options.list(this.items, this.translate);
    }
    this.status = this.initial;
    super.ngOnInit();
  }

  value() {
    return this.status;
  }

  filtered(): string {
    return this.status && (collections.find(this.items, { id: this.status }) || {}).name;
  }

}
