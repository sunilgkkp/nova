import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Checklist } from 'src/controller/utils/CheckList';
import { Collections } from 'src/controller/utils/Collections';
import { Options } from 'src/controller/utils/Options';
import { FilterBase } from '../filter.base';

@Component({
  selector: 'app-filter-buttons',
  templateUrl: './filter-buttons.component.html',
  styleUrls: ['./filter-buttons.component.scss'],
})
export class FilterButtonsComponent extends FilterBase {

  @Input('options') items: any;
  @Input('name') name: string;
  @Input('context') context: string;
  @Input('set') initial: any;
  @Output('init') init: EventEmitter<any> = new EventEmitter();
  @Output('action') action: EventEmitter<any> = new EventEmitter();

  id: any;
  list: any;

  constructor(
    protected translate: TranslateService
  ) {
    super(translate);
  }

  ngOnInit() {
    if (typeof this.items === 'string') {
      this.list = Options.list(this.items, this.translate);
      this.items = Checklist.prepare(this.list);
      (this.initial || []).forEach((selected: string) => this.items[selected] = true);
    }
    super.ngOnInit();
  }

  value() {
    return Checklist.selected(this.items);
  }

  filtered(): string {
    const selected = Checklist.selected(this.items);
    return selected.length && selected.map((id) => Collections.find(this.list, { id }).name).join(', ');
  }

  clear() {
    Checklist.reset(this.items);
    this.set();
  }

}
