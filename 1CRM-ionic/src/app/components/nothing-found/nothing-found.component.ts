import { Component, Input, OnInit } from '@angular/core';
import { IonTabs } from '@ionic/angular';
import { EventsService } from 'src/controller/services/events/events.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';
import { StorageService } from 'src/controller/services/storage/storage.service';

@Component({
  selector: 'app-nothing-found',
  templateUrl: './nothing-found.component.html',
  styleUrls: ['./nothing-found.component.scss'],
})
export class NothingFoundComponent implements OnInit {


  @Input() context: string;
  @Input() values?: any;
  @Input() icon?: string;
  @Input() redirect: string;
  firstname: string;
  constructor(
    private router: NavigationService,
    private events: EventsService,
    private tabs: IonTabs,
    public storage: StorageService,
  ) {
    this.firstname = storage.userName
  }

  ngOnInit() { }

  redirectClick() {
    if (this.redirect == 'jobs:matched') {
      let params: any = { name: 'tabChange', data: { tabName: 'jobs', params: 'matched' } };
      this.events.publish(params);
    } else if (this.redirect == 'certificates:all') {
      let params: any = { name: 'tabChange', data: { tabName: 'certificates', params: 'all' } };
      this.events.publish(params);
    } else if (this.redirect == 'bills:preparation') {
      let params: any = { name: 'tabChange', data: { tabName: 'bills', params: 'preparation' } };
      this.events.publish(params);
    } else if (this.redirect == 'bills:invoice') {
      let params: any = { name: 'tabChange', data: { tabName: 'bills', params: 'create_invoice' } };
      this.events.publish(params);
    } else if (this.redirect == 'assignments') {
      this.tabs.select('assignments');
    }
  }

}
