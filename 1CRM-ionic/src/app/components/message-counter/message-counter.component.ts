import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-message-counter',
  templateUrl: './message-counter.component.html',
  styleUrls: ['./message-counter.component.scss'],
})
export class MessageCounterComponent implements OnInit {

  @Input() visible: string = 'new';
  @Input() strategy: { listen?: string } = { listen: 'messages:check' };

  public total: number = 0;

  constructor() { }

  ngOnInit() { }

}
