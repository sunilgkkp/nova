import { Component, Input, OnInit } from '@angular/core';
import { PushService } from 'src/controller/services/push/push.service';

@Component({
  selector: 'app-push-notify-toggle',
  templateUrl: './push-notify-toggle.component.html',
  styleUrls: ['./push-notify-toggle.component.scss'],
})
export class PushNotifyToggleComponent implements OnInit {

  @Input() type: string;
  @Input() label: string;
  isSubscribed: boolean;

  constructor(private push: PushService) { }

  ngOnInit() {
    this.push.isSubscribed(this.type).then((res) => {
      this.isSubscribed = res;
    });
  }

  handlePush() {
    this.push.set(this.type, this.isSubscribed);
  }

}
