import { Component, Input, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { IFilter } from 'src/controller/services/certificates/certificates.service';

@Component({
  selector: 'app-back-button',
  templateUrl: './back-button.component.html',
  styleUrls: ['./back-button.component.scss'],
})
export class BackButtonComponent implements OnInit {

  @Input('label') label: string;
  public filtered: IFilter = {};
  constructor(
    private navCtrl: NavController,
    private modalCtrl: ModalController
  ) {
  }

  ngOnInit() {
    console.log('filtered => ', this.filtered);

  }

  async back() {
    let isModal = await this.modalCtrl.getTop();
    if (isModal == undefined) {
      this.navCtrl.back();
    } else {
      this.modalCtrl.dismiss();
    }
  }

}
