import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    children: [
      {
        path: '',
        redirectTo: 'tabs',
        pathMatch: 'full'
      },
      {
        path: 'tabs',
        loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
      },
      {
        path: 'jobs-list',
        loadChildren: () => import('./pages/jobs/jobs-list/jobs-list.module').then(m => m.JobsListPageModule)
      },
      {
        path: 'bills',
        loadChildren: () => import('./pages/bills/bills/bills.module').then(m => m.BillsPageModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./pages/tabs/pages/dashboard/dashboard.module').then(m => m.DashboardPageModule)
      },
      {
        path: 'assignment-card',
        loadChildren: () => import('./pages/tabs/pages/assignments/assignment-card/assignment-card.module').then(m => m.AssignmentCardPageModule)
      },
      {
        path: 'assignment-detail',
        loadChildren: () => import('./pages/tabs/pages/assignments/assignment-detail/assignment-detail.module').then(m => m.AssignmentDetailPageModule)
      },
      {
        path: 'survey',
        loadChildren: () => import('./pages/survey/survey.module').then(m => m.SurveyPageModule)
      },
      {
        path: 'job-card',
        loadChildren: () => import('./pages/jobs/job-card/job-card.module').then(m => m.JobCardPageModule)
      },
      {
        path: 'job-detail',
        loadChildren: () => import('./pages/jobs/job-detail/job-detail.module').then(m => m.JobDetailPageModule)
      },
      {
        path: 'invoices',
        loadChildren: () => import('./pages/bills/invoices/invoices/invoices.module').then(m => m.InvoicesPageModule)
      },
      {
        path: 'createinvoice',
        loadChildren: () => import('./pages/bills/invoices/createinvoice/createinvoice.module').then(m => m.CreateinvoicePageModule)
      },
      {
        path: 'preparation',
        loadChildren: () => import('./pages/bills/preparation/preparation/preparation.module').then(m => m.PreparationPageModule)
      },
      {
        path: 'preparation',
        loadChildren: () => import('./pages/bills/preparation/preparation/preparation.module').then(m => m.PreparationPageModule)
      },
      {
        path: 'preparation-details',
        loadChildren: () => import('./pages/bills/preparation/preparation-details/preparation-details.module').then(m => m.PreparationDetailsPageModule)
      },
      {
        path: 'create-invoices-details',
        loadChildren: () => import('./pages/bills/invoices/create-invoices-details/create-invoices-details.module').then(m => m.CreateInvoicesDetailsPageModule)
      }
    ]
  },
  {
    path: 'certificates',
    loadChildren: () => import('./pages/certificates/certificates/certificates.module').then(m => m.CertificatesPageModule)
  },
  {
    path: 'training-details',
    loadChildren: () => import('./pages/certificates/training-details/training-details.module').then(m => m.TrainingDetailsPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./pages/settings/settings.module').then(m => m.SettingsPageModule)
  },
  {
    path: 'invoice-details',
    loadChildren: () => import('./pages/bills/invoices/invoice-details/invoice-details.module').then(m => m.InvoiceDetailsPageModule)
  },
  {
    path: 'create-message',
    loadChildren: () => import('./pages/messages/create-message/create-message.module').then(m => m.CreateMessagePageModule)
  },
  {
    path: 'message-list',
    loadChildren: () => import('./pages/messages/message-list/message-list.module').then(m => m.MessageListPageModule)
  },
  {
    path: 'revenu-report-detail',
    loadChildren: () => import('./pages/bills/preparation/revenu-report-detail/revenu-report-detail.module').then(m => m.RevenuReportDetailPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('../profile/profile.module').then(m => m.ProfilePageModule)
  },  {
    path: 'to-do',
    loadChildren: () => import('./pages/to-do/to-do.module').then( m => m.ToDoPageModule)
  },
  {
    path: 'account-setting',
    loadChildren: () => import('./pages/account-setting/account-setting.module').then( m => m.AccountSettingPageModule)
  },
  {
    path: 'job-applied',
    loadChildren: () => import('./pages/jobs/job-applied/job-applied.module').then( m => m.JobAppliedPageModule)
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule { }
