import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

import { HomePageRoutingModule } from './home-routing.module';
import { HomePage } from './home.page';
import { SharedModule } from '../shared/shared/shared.module';
import { PrepartionDetailModule } from './pages/bills/preparation/preparation/preparation-detail/prepartion-detail.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    HomePageRoutingModule,
    PrepartionDetailModule
  ],
  declarations: [HomePage]
})
export class HomePageModule { }
