import { Component } from '@angular/core';
import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { FunctionService } from 'src/controller/services/function/function.service';
import { GeneralService } from 'src/controller/services/general/general.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';
import Collection from 'src/controller/utils/Collection';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  menuItem: any;
  constructor(
    private navCtrl: NavigationService,
    private generalService: GeneralService,
    private auth: AuthenticationService,
    public fun: FunctionService
  ) {
    this.menuItem = Collection.menuData;
  }

  goto(url) {
    this.navCtrl.goTo(url);
    this.fun.closeMenu();
  }

  logout() {
    this.generalService.confirmAlert({
      header: 'auth.logout.confirm',
      cancel: true,
      confirm: true,
      onConfirm: () => {
        this.auth.logout()
      },
    });
  }

}
