import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobCardPage } from './job-card.page';

const routes: Routes = [
  {
    path: '',
    component: JobCardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobCardPageRoutingModule {}
