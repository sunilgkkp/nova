import { Collections } from 'src/controller/utils/Collections';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IonItemSliding, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { GeneralService } from 'src/controller/services/general/general.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';
import { ProfileService } from 'src/controller/services/profile/profile.service';
import { JobService } from 'src/controller/services/jobPosting/job.service';
import { JobDetailComponent } from 'src/app/components/job-detail/job-detail.component';

export enum JobState {
  Matching,
  NotMatching,
  Applied,
}

export interface IJobStatus {
  state?: JobState;
  class?: {
    processing?: boolean;
    matching?: boolean;
    [key: string]: boolean;
  };
}

export interface IJobDetails {
  matching?: boolean;
  mismatched?: any;
  tenders?: any[];
  offered?: boolean;
  shortTitle?: string;
  client?: any;
  category?: string;
  range?: any;
  state?: JobState;
  site?: any;
  id?: number;
  assignment?: any;
  contract_type_identifier?: string;
  contractType?: string;
}

const actionDelay: number = 3000;

@Component({
  selector: 'app-job-card',
  templateUrl: './job-card.page.html',
  styleUrls: ['./job-card.page.scss'],
})
export class JobCardPage implements OnInit {

  @Input() job: IJobDetails;
  @Output() removed: EventEmitter<any> = new EventEmitter();
  @Output() changed: EventEmitter<any> = new EventEmitter();

  process: boolean = false;
  pending: any = undefined;
  status: IJobStatus = {};
  delay: number = actionDelay / 1000;
  profile: any;


  constructor(
    private generalService: GeneralService,
    private profileService: ProfileService,
    public navCrtl: NavigationService,
    private translate: TranslateService,
    private modalCtrl: ModalController,
    private jobsService: JobService
  ) { }

  ngOnInit() {
    // this.profileService.getCurrentUser().then((user) => {
    //   this.profile = user;
    // });
    this.setStatus();
  }

  private setStatus() {
    this.status = this.getStatus(this.job);

  }

  getStatus(job: any): IJobStatus {
    const state: JobState = job.matching ? JobState.Matching : JobState.NotMatching;
    const status: any = {
      state,
      class: {
        matching: state === JobState.Matching,
        mismatched: state === JobState.NotMatching,
      },
    };
    Object.entries(job.mismatched).forEach((entry) => {
      const [key] = entry;
      status.class[`mismatched-${key}`] = true;
    });

    if (job.offered) {
      status.state = JobState.Applied;
    }
    return status;
  }

  async showDetails() {
    // console.log('this.job.matching => ', this.job.matching);
    // if (this.job.matching) {
    let j: any = this.job;
    const modal = await this.modalCtrl.create({
      component: JobDetailComponent,
      cssClass: 'full',
      componentProps: { job: j, operations: this }
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data === 'accepted-partially') {
      this.changed.emit(this.job);
    } else if (data) {
      this.removed.emit(this.job);
    }
    // } else {
    //   this.generalService.confirmAlert({
    //     header: 'jobs.mismatched.title',
    //     message: this.mismatchedMsg(),
    //     cancel: true,
    //   });
    // }
  }

  private mismatchedMsg(): string {
    return Object.entries(this.job.mismatched).map((entry: any[]) => {
      const [key, value] = entry;
      const criteria = this.translate.instant('jobs.mismatched.' + key);
      const values = (!Array.isArray(value) ? [value] : value).join(', ');
      return `<div>${criteria}</div><div>${values}</div>`;
    }).join('');
  }


  cancel(event?: MouseEvent, item?: IonItemSliding): boolean {
    if (event) {
      event.stopPropagation();
    }
    if (item) {
      item.close();
    }
    if (this.pending) {
      clearTimeout(this.pending);
      this.pending = undefined;
    }
    this.processing(false);
    return true;
  }

  /**
   * Sends accept offer to api (within confirmation implemented in operations)
   *
   * @param item
   * @param event
  */
  accept(event?: MouseEvent, item?: IonItemSliding) {
    if (event) {
      this.cancel(event, item);
    }
    if (!this.processing()) {
      const ids = Collections.ids(this.job.tenders);
      this.processing(true);
      this.acceptOffers(this.job, ids).then(() => {
        this.cancel();
        this.removed.emit(this.job);
      }).catch(() => {
        this.cancel();
      });
    }
  }

  /**
  * Setter/getter of processing item (API operations)
  *
  * @param set Processing flag
  */
  private processing(set?: boolean) {
    if (set === undefined) {
      return this.process;
    } else {
      this.process = set;
      if (this.status.class) {
        this.status.class.processing = set;
      }
    }
  }

  /**
  * Shows alert and sends offers on confirm
  *
  * @param job
  * @param ids Assignment ids
  */
  acceptOffers(job: any, ids: number[]): Promise<any> {
    return this.confirmation('accept', job, ids);
  }

  /**
   * Displays confirmation and performs action
   *
   * @param type Operation - accept or reject
   * @param job
   * @param ids Assignment ids
  */
  private confirmation(type: string, job: any, ids: number[]) {
    return new Promise((resolve, reject) =>
      this.generalService.confirmAlert({
        context: 'jobs.offer',
        header: 'jobs.mismatched.title',
        message: `${type}.message.` + (ids.length === 1 ? 'single' : 'multi'),
        item: Object.assign(job, { selected: ids.length }),
        onConfirm: () => this.tenders(type, ids).then(resolve, reject),
        cancel: true,
      })
    );
  }

  /**
   * Internal method creating bulk of requests
   *
   * @param type Operation - accept or reject
   * @param params Common params
   * @param ids Assignment ids
  */
  private tenders(type: string, ids: number[]): Promise<any> {
    let requests: any = [];
    const params = { freelancer_id: this.profile.roleId() };
    if (type === 'accept') {
      const data = ids.map((id) => Object.assign({ tender_id: id }, params));
      requests = this.jobsService.submitOffers(data), 'jobs.offer.accept';
    } else {
      requests = ids.map((id) => {
        const data = Object.assign({ id, tender_id: id, reason: 'FLAPP' }, params);
        return this.jobsService.rejectTender(params.freelancer_id, data)
      });
    }
    return Promise.all(requests);
  }


}
