import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobCardPageRoutingModule } from './job-card-routing.module';
import { JobCardPage } from './job-card.page';
import { SharedModule } from 'src/app/shared/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    JobCardPageRoutingModule
  ],
  declarations: [JobCardPage]
})
export class JobCardPageModule { }
