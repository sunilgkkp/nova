import { JobAppliedPage } from './../job-applied/job-applied.page';
import { JobCardPage } from './../job-card/job-card.page';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobsListPageRoutingModule } from './jobs-list-routing.module';
import { JobsListPage } from './jobs-list.page';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'src/app/shared/shared/shared.module';
import { FilterModule } from 'src/app/components/filter/filter.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    FilterModule,
    TranslateModule,
    JobsListPageRoutingModule
  ],
  declarations: [JobsListPage, JobCardPage, JobAppliedPage]
})
export class JobsListPageModule { }
