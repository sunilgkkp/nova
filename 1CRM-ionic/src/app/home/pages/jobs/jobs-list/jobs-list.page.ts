import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { IFilterBarItems } from 'src/app/components/filter/filter.component';
import { IApiUser } from 'src/controller/apiAppData/apiUser/user-data.service';
import { IFilter } from 'src/controller/services/certificates/certificates.service';
import { FunctionService } from 'src/controller/services/function/function.service';
import { JobMatch, JobService } from 'src/controller/services/jobPosting/job.service';
import { ProfileService } from 'src/controller/services/profile/profile.service';
import Collection from 'src/controller/utils/Collection';
import { Collections } from 'src/controller/utils/Collections';

@Component({
  selector: 'app-jobs-list',
  templateUrl: './jobs-list.page.html',
  styleUrls: ['./jobs-list.page.scss'],
})
export class JobsListPage implements OnInit {

  checkBoxList: any = [];
  public profile: IApiUser;
  public items: any = [];
  public moreItems: any = [];
  public type: string;
  public filtered: IFilter = {};
  public selectedSegment: string = '';
  public filters: IFilterBarItems;
  filterOpened: boolean = true;
  appliedFilter: boolean = true;
  public loading: any;
  context: string;
  public currentPage: number = 1;
  constructor(
    private jobsService: JobService,
    private loadingCtrl: LoadingController,
    public fun: FunctionService,
    private translate: TranslateService,
    private ref: ChangeDetectorRef,
    private route: ActivatedRoute,
    private profileService: ProfileService,
  ) {
    this.checkBoxList = Collection.filterCheckbox;
  }

  ngOnInit() {
    let getParam: string = 'matched';
    this.route.params.subscribe((params: Params) => {
      getParam = params.id ? params.id : 'matched';
    });
    this.selectedSegment = getParam
    this.type = getParam;
    this.profileService.getCurrentUser().then((user) => {
      this.profile = user;
      this.initFilters();
      this.fetch([], 12, this.currentPage);
    });
  }

  initFilters() {
    if (this.selectedSegment === 'offers') {
      this.filters = {
        state: { set: ['pending'], type: 'buttons', options: 'jobs.offer.states' },
        contractType: { type: 'select', options: [], set: undefined, none: true },
        search: { type: 'search', set: '' },
      };
      this.context = 'offers';
    } else {
      this.filters = {
        search: { type: 'search', set: '' },
        dates: { type: 'daterange', set: { start: null, end: null } },
        postcodes: { type: 'range', set: { min: null, max: null } },
        contractType: { type: 'select', options: [], set: undefined, none: true },
        certificate: { type: 'select', options: [], set: undefined, none: true },
        checkbox: { type: 'selectCheckbox', options: this.checkBoxList, set: undefined },
      };
      this.context = 'jobs';
      this.jobsService.getAllCertificates().then((data: any) => {
        this.filters.certificate.options = data.data.map((cert) => Collections.only(cert, ['id', 'name', 'teaser', 'identifier']));
      });
    }
    this.jobsService.getAllContractTypes().then((data: any) => {
      this.filters.contractType.options = data.data.map((contract) => Collections.only(contract, ['id', 'name', 'teaser', 'identifier']));
    });
  }

  async presentLoading(text: string) {
    this.loading = await this.loadingCtrl.create({
      message: this.translate.instant(text),
      backdropDismiss: false,
    });
    return await this.loading.present();
  }

  fetch(loaded?: any[], load: number = 12, page = 1): any {
    if (page == 1) {
      this.items = [];
      this.moreItems = [];
    }
    this.presentLoading('common.fetching-data').then(() => {
      if (this.selectedSegment === 'offers') {
        return this.jobsService.offers(this.selectedSegment, this.profile?.roleId(), loaded && loaded.length, load, this.filtered, page).then((items) => {
          this.setItems(loaded, items);
          this.loading.dismiss();
        }).catch((error) => {
          this.loading.dismiss();
          console.log('error', error);
        });
      }
      else if (this.selectedSegment === 'invited') {
        return this.jobsService.jobInviteData().then((res: any) => {
          if (res) {
            this.loading.dismiss();
          }
        }).catch((error) => {
          this.loading.dismiss();
          console.log('error', error);
        });
      }
      else {
        const matching = this.selectedSegment === 'matched' ? JobMatch.Yes : JobMatch.No;
        return this.jobsService.list(this.selectedSegment, matching, loaded && loaded.length, load, this.filtered, page).then((items) => {
          this.setItems(loaded, items);
          this.loading.dismiss();
        }).catch((error) => {
          this.loading.dismiss();
          console.log('error', error);
        });
      }
    }).catch((err) => {
      console.log('items', err);
      this.loading.dismiss();
    });
  }

  protected setItems(current: any[], items: any, property: string = 'id') {
    // this.items = Collections.unique((current ? this.items : []).concat(items.data || items), property);
    this.items = items.data
    this.moreItems = !items.meta || this.items.length < items.meta.pagination.total;
  }

  onFilterOpened(isOpen) {
    this.filterOpened = isOpen;
  }

  doRefresh(event?: any) {
    setTimeout(() => {
      event.target.complete();
      this.currentPage = 1;
      this.fetch([], 12, this.currentPage);
    }, 2000);
  }

  filter(filtered: IFilter) {
    this.filtered = filtered;
    this.currentPage = 1;
    this.fetch([], 12, this.currentPage);
  }

  onChange(job: any, limit: any, include: string) {
    return this.jobsService.get(include, limit = 5).then((data) => {
      this.items[this.items.indexOf(job)] = data;
      this.ref.detectChanges();
    });
  }

  onRemove(job: any) {
    this.items.splice(this.items.indexOf(job), 1);
    this.ref.detectChanges();
  }

  segmentChanged(event): void {
    this.selectedSegment = event.detail.value;
    this.filtered = null;
    this.type = event.detail.value;
    this.currentPage = 1;
    this.fetch([], 12, this.currentPage);
  }

  loadData(event, loaded, load) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();
      this.currentPage = this.currentPage + 1;
      this.fetch(loaded, load, this.currentPage);
    }, 500);
  }
}
