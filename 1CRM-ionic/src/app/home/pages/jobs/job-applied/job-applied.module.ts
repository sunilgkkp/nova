import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobAppliedPageRoutingModule } from './job-applied-routing.module';
import { JobAppliedPage } from './job-applied.page';
import { SharedModule } from 'src/app/shared/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    JobAppliedPageRoutingModule
  ],
  declarations: [JobAppliedPage]
})
export class JobAppliedPageModule { }
