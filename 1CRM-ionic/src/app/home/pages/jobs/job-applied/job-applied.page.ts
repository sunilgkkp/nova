import { AppliedDetailsComponent } from './../../../../components/applied-details/applied-details.component';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IonItemSliding, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { GeneralService } from 'src/controller/services/general/general.service';
import { JobService } from 'src/controller/services/jobPosting/job.service';
import { Collections } from 'src/controller/utils/Collections';
import { IJobDetails, IJobStatus, JobState } from '../job-card/job-card.page';

const actionDelay: number = 3000;

@Component({
  selector: 'app-job-applied',
  templateUrl: './job-applied.page.html',
  styleUrls: ['./job-applied.page.scss'],
})
export class JobAppliedPage implements OnInit {

  @Input() offers: IJobDetails;
  @Output() removed: EventEmitter<any> = new EventEmitter();
  @Output() changed: EventEmitter<any> = new EventEmitter();

  process: boolean = false;
  pending: any = undefined;
  status: IJobStatus = {};
  delay: number = actionDelay / 1000;
  profile: any;

  constructor(
    private translate: TranslateService,
    private generalService: GeneralService,
    private jobsService: JobService,
    private modalCtrl: ModalController,
  ) { }

  ngOnInit() {
    this.setStatus();
  }

  private setStatus() {
    this.status = this.getStatus(this.offers);

  }

  getStatus(job: any): IJobStatus {
    const state: JobState = job.matching ? JobState.Matching : JobState.NotMatching;
    const status: any = {
      state,
      class: {
        matching: state === JobState.Matching,
        mismatched: state === JobState.NotMatching,
      },
    };
    Object.entries(job.mismatched).forEach((entry) => {
      const [key] = entry;
      status.class[`mismatched-${key}`] = true;
    });

    if (job.offered) {
      status.state = JobState.Applied;
    }
    return status;
  }

  private mismatchedMsg(): string {
    return Object.entries(this.offers.mismatched).map((entry: any[]) => {
      const [key, value] = entry;
      const criteria = this.translate.instant('jobs.mismatched.' + key);
      const values = (!Array.isArray(value) ? [value] : value).join(', ');
      return `<div>${criteria}</div><div>${values}</div>`;
    }).join('');
  }

  cancel(event?: MouseEvent, item?: IonItemSliding): boolean {
    if (event) {
      event.stopPropagation();
    }
    if (item) {
      item.close();
    }
    if (this.pending) {
      clearTimeout(this.pending);
      this.pending = undefined;
    }
    this.processing(false);
    return true;
  }

  /**
   * Sends accept offer to api (within confirmation implemented in operations)
   *
   * @param item
   * @param event
  */
  accept(event?: MouseEvent, item?: IonItemSliding) {
    if (event) {
      this.cancel(event, item);
    }
    if (!this.processing()) {
      const ids = Collections.ids(this.offers.tenders);
      this.processing(true);
      this.acceptOffers(this.offers, ids).then(() => {
        this.cancel();
        this.removed.emit(this.offers);
      }).catch(() => {
        this.cancel();
      });
    }
  }

  async showDetails() {
    if (this.offers.matching) {
      let j: any = this.offers;
      const modal = await this.modalCtrl.create({
        component: AppliedDetailsComponent,
        cssClass: 'full',
        componentProps: { offers: j, operations: this }
      });
      await modal.present();
      const { data } = await modal.onWillDismiss();
      if (data === 'accepted-partially') {
        this.changed.emit(this.offers);
      } else if (data) {
        this.removed.emit(this.offers);
      }
    } else {
      this.generalService.confirmAlert({
        header: 'jobs.mismatched.title',
        message: this.mismatchedMsg(),
        cancel: true,
      });
    }
  }

  /**
* Setter/getter of processing item (API operations)
*
* @param set Processing flag
*/
  private processing(set?: boolean) {
    if (set === undefined) {
      return this.process;
    } else {
      this.process = set;
      if (this.status.class) {
        this.status.class.processing = set;
      }
    }
  }

  /**
  * Shows alert and sends offers on confirm
  *
  * @param job
  * @param ids Assignment ids
  */
  acceptOffers(job: any, ids: number[]): Promise<any> {
    return this.confirmation('accept', job, ids);
  }

  /**
 * Displays confirmation and performs action
 *
 * @param type Operation - accept or reject
 * @param job
 * @param ids Assignment ids
*/
  private confirmation(type: string, job: any, ids: number[]) {
    return new Promise((resolve, reject) =>
      this.generalService.confirmAlert({
        context: 'jobs.offer',
        header: 'jobs.mismatched.title',
        message: `${type}.message.` + (ids.length === 1 ? 'single' : 'multi'),
        item: Object.assign(job, { selected: ids.length }),
        onConfirm: () => this.tenders(type, ids).then(resolve, reject),
        cancel: true,
      })
    );
  }

  /**
   * Internal method creating bulk of requests
   *
   * @param type Operation - accept or reject
   * @param params Common params
   * @param ids Assignment ids
  */
  private tenders(type: string, ids: number[]): Promise<any> {
    let requests: any = [];
    const params = { freelancer_id: this.profile.roleId() };
    if (type === 'accept') {
      const data = ids.map((id) => Object.assign({ tender_id: id }, params));
      requests = this.jobsService.submitOffers(data), 'jobs.offer.accept';
    } else {
      requests = ids.map((id) => {
        const data = Object.assign({ id, tender_id: id, reason: 'FLAPP' }, params);
        return this.jobsService.rejectTender(params.freelancer_id, data)
      });
    }
    return Promise.all(requests);
  }

}
