import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { UserDataService } from 'src/controller/apiAppData/apiUser/user-data.service';
import { GeneralService } from 'src/controller/services/general/general.service';
import { MessageService } from 'src/controller/services/messageService/message.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';
import { Checklist } from 'src/controller/utils/CheckList';
import { Format } from 'src/controller/utils/format';
import { IJobDetails } from '../job-card/job-card.page';

@Component({
  selector: 'app-job-detail',
  templateUrl: './job-detail.page.html',
  styleUrls: ['./job-detail.page.scss'],
})
export class JobDetailPage implements OnInit {

  details: IJobDetails;
  tendersData: any = {};
  offered: boolean;
  restricted: string;
  selectedAll = false;
  isProcessing: boolean;
  operations: any;
  constructor(
    private user: UserDataService,
    private params: NavParams,
    public navCtrl: NavigationService,
    private translate: TranslateService,
    public modalCtrl: ModalController,
    private generalService: GeneralService,
    private messagesService: MessageService
  ) {
  }

  ngOnInit() {
    this.operations = this.params.get('operations');
    this.details = this.params.get('job');
    this.tendersData = Checklist.prepare(this.details.tenders);
    this.restricted = this.user.get().isRestricted(this.details.contract_type_identifier);
  }

  selected(): number[] {
    return Checklist.selected(this.tendersData);
  }

  toggleOne() {
    this.selectedAll = this.selected().length === this.details.tenders.length;
  }

  toggleAll() {
    if (this.selected().length === this.details.tenders.length) {
      Checklist.reset(this.tendersData);
    } else {
      Checklist.set(this.tendersData);
    }
  }

  info(tender: any) {
    const incentives = tender.snapshots.incentive_model;
    const costs: Array<{ name: string, value: string }> = tender.snapshots.assignment.additional_costs;
    const msg = [
      incentives && ['label', 'checkin', 'sales_report', 'picture_documentation']
        .map((key) => this.infoLine(this.translate.instant(`assignments.details.incentives.${key}`), incentives[key])).join(''),
      costs && costs.length && [this.infoLine(this.translate.instant('assignments.details.costs.label'))]
        .concat(costs.map((cost) => this.infoLine(cost.name, cost.value))).join(''),
    ];

    this.generalService.confirmAlert({
      context: 'assignments',
      title: 'details.additional',
      message: msg.filter(Boolean).join('<div class="divider"></div>'),
      item: tender,
      confirm: true
    });
  }

  private infoLine(key: string, value?: string): string {
    const val = (value && Format.numbers(value) + '€') || '';
    return `<div><span>${key}</span><span>${val}</span></div>`;
  }

  acceptOffers() {
    if (!this.isProcessing && this.operations) {
      const type = 'accepted' + (this.selectedAll ? '-all' : '-partially');
      this.isProcessing = true;
      this.accept(this.details, this.selected()).then(() => {
        this.dismiss(type);
      }).catch(() => {
        this.isProcessing = false;
      });
    }
  }

  dismiss(operation?: string) {
    this.modalCtrl.dismiss(operation);
  }

  /**
   * Shows alert and sends offers on confirm
   *
   * @param job
   * @param ids Assignment ids
   */
  accept(job: any, ids: number[]): Promise<any> {
    return this.confirmation('accept', job, ids);
  }

  /**
    * Reject all offers
    *
    * @param job
    * @param ids Assignment ids
  */
  reject(job: any, ids: number[], confirm: boolean = true): Promise<any> {
    if (confirm) {
      return this.confirmation('reject', job, ids);
    } else {
      return this.tenders('reject', ids);
    }
  }


  /**
   * Displays confirmation and performs action
   *
   * @param type Operation - accept or reject
   * @param job
   * @param ids Assignment ids
  */
  private confirmation(type: string, job: any, ids: number[]) {
    return new Promise((resolve, reject) =>
      this.generalService.confirmAlert({
        context: 'jobs.offer',
        title: `${type}.title`,
        message: `${type}.message.` + (ids.length === 1 ? 'single' : 'multi'),
        item: Object.assign(job, { selected: ids.length }),
        confirm: true,
        cancel: true,
        onConfirm: () => {
          this.tenders(type, ids).then(resolve, reject)
        },
      }),
    );
  }

  /**
   * Internal method creating bulk of requests
   *
   * @param type Operation - accept or reject
   * @param params Common params
   * @param ids Assignment ids
  */
  private tenders(type: string, ids: number[]): Promise<any> {
    let requests = [];
    const params = { freelancer_id: this.user.get().roleId() };
    if (type === 'accept') {
      const data = ids.map((id) => Object.assign({ tender_id: id }, params));
    } else {
      requests = ids.map((id) => {
        const data = Object.assign({ id, tender_id: id, reason: 'FLAPP' }, params);
      });
    }
    return Promise.all(requests);
  }



}
