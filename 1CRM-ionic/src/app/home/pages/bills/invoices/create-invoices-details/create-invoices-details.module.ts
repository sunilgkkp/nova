import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateInvoicesDetailsPageRoutingModule } from './create-invoices-details-routing.module';

import { CreateInvoicesDetailsPage } from './create-invoices-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateInvoicesDetailsPageRoutingModule
  ],
  declarations: [CreateInvoicesDetailsPage]
})
export class CreateInvoicesDetailsPageModule {}
