import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateInvoicesDetailsPage } from './create-invoices-details.page';

const routes: Routes = [
  {
    path: '',
    component: CreateInvoicesDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateInvoicesDetailsPageRoutingModule {}
