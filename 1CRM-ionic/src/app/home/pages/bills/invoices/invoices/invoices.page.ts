import { Component, OnInit } from '@angular/core';
import { IFilterBarItems } from 'src/app/components/filter/filter.component';
import { IApiUser } from 'src/controller/apiAppData/apiUser/user-data.service';
import { BillService } from 'src/controller/services/billService/bill.service';
import { IFilter } from 'src/controller/services/certificates/certificates.service';
import { FunctionService } from 'src/controller/services/function/function.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';
import { ProfileService } from 'src/controller/services/profile/profile.service';
import { Collections } from 'src/controller/utils/Collections';
import { InvoiceDetailsPage } from '../invoice-details/invoice-details.page';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.page.html',
  styleUrls: ['./invoices.page.scss'],
})
export class InvoicesPage implements OnInit {

  public filters: IFilterBarItems;
  public filtered: IFilter = {};
  filterOpened: boolean = true;
  public profile: IApiUser;
  public loading: any;
  public items: any = [];
  public moreItems: any = [];
  public currentPage: number = 1;


  constructor(
    private profileService: ProfileService,
    private billsService: BillService,
    public fun: FunctionService,
    public navCtrl: NavigationService
  ) { }

  ngOnInit() {
    this.profileService.getCurrentUser().then((user) => {
      this.profile = user;
      this.initFilters();
      this.fetch([], 10, this.currentPage);
    });
    this.initFilters();
  }

  fetch(loaded?: any[], load: number = 10, page = 1): any {
    if (page == 1) {
      this.items = [];
      this.moreItems = [];
    }
    const offset = loaded && loaded.length;
    this.fun.presentLoading('common.fetching-data').then(() => {
      return this.billsService.list(this.profile.roleId(), offset, load, this.filtered, page).then((items) => {
        this.setItems(loaded, items);
      }).catch((error) => {
        console.log('error', error);
      });
    }).catch((err) => {
      console.log('error', err);
    });
  }

  protected setItems(current: any[], items: any, property: string = 'id') {
    this.items = Collections.unique((current ? this.items : []).concat(items.data || items), property);
    this.moreItems = !items.meta || this.items.length < items.meta.pagination.total;
  }

  initFilters() {
    this.filters = {
      search: { type: 'search', set: '' },
      state: { type: 'select', set: undefined, none: true, options: 'bills.invoices.states' },
      jobs: { type: 'jobs', set: '' },
      dates: { type: 'daterange', set: { start: null, end: null } },
    };
  }

  async showDetails(invoice: any) {
    let d: any = { invoice, profile: this.profile }
    const modal = this.navCtrl.popup(InvoiceDetailsPage);
    if (modal) {
      this.doRefresh();
    }
  }

  filter(filtered: IFilter) {
    this.filtered = filtered;
    this.fetch([], 10, this.currentPage);
  }

  onFilterOpened(isOpen) {
    this.filterOpened = isOpen;
  }

  filterCollapses() {
    this.filterOpened = !this.filterOpened;
  }


  doRefresh(event?: any) {
    setTimeout(() => {
      event.target.complete();
      this.currentPage = 1;
      this.fetch([], 10, this.currentPage);
    }, 2000);
  }

  loadData(event, loaded, load) {
    setTimeout(() => {
      event.target.complete();
      this.currentPage = this.currentPage + 1;
      this.fetch(loaded, load, this.currentPage);
    }, 500);
  }
}
