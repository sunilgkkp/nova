import { Collections } from './../../../../../../controller/utils/Collections';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IApiUser } from 'src/controller/apiAppData/apiUser/user-data.service';
import { BillService } from 'src/controller/services/billService/bill.service';
import { FunctionService } from 'src/controller/services/function/function.service';
import { ProfileService } from 'src/controller/services/profile/profile.service';
import { CreateInvoicesDetailsPageModule } from '../create-invoices-details/create-invoices-details.module';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';

@Component({
  selector: 'app-createinvoice',
  templateUrl: './createinvoice.page.html',
  styleUrls: ['./createinvoice.page.scss'],
})
export class CreateinvoicePage implements OnInit {

  jobs: any[] = undefined;
  public loading: any;
  public profile: IApiUser;
  public items: any = [];
  public moreItems: any = [];
  public currentPage: number = 1;

  constructor(
    private profileService: ProfileService,
    private translate: TranslateService,
    private billsService: BillService,
    public navCtrl: NavigationService,
    private fun: FunctionService
  ) { }

  ngOnInit() {
    this.profileService.getCurrentUser().then((user) => {
      this.profile = user;
      this.fetch([], 10, this.currentPage);
    });
  }

  fetch(loaded?: any[], load: number = 10, page = 1): any {
    if (page == 1) {
      this.items = [];
      this.moreItems = [];
    }
    this.fun.presentLoading('common.fetching-data').then(() => {
      return this.billsService.jobs(this.profile.roleId(), 'invoiceable', loaded && loaded.length, load, page).then((items) => {
        this.setItems(loaded, items);
      }).catch((error) => {
        console.log('error', error);
      });
    }).catch((err) => {
      console.log('error', err);
    });
  }

  protected setItems(current: any[], items: any, property: string = 'id') {
    this.items = Collections.unique((current ? this.items : []).concat(items.data || items), property);
    this.moreItems = !items.meta || this.items.length < items.meta.pagination.total;
  }

  doRefresh(event?: any) {
    setTimeout(() => {
      event.target.complete();
      this.currentPage = 1;
      this.fetch([], 10, this.currentPage);
    }, 2000);
  }

  async showDetails(selected: any) {
    let d: any = { job: selected, profile: this.profile }
    this.navCtrl.popup(CreateInvoicesDetailsPageModule);
    if (d) {
      this.doRefresh();
    }
  }


}
