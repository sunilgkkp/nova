import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateinvoicePageRoutingModule } from './createinvoice-routing.module';

import { CreateinvoicePage } from './createinvoice.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateinvoicePageRoutingModule
  ],
  declarations: [CreateinvoicePage]
})
export class CreateinvoicePageModule {}
