import { PreparationDetailComponent } from './preparation-detail.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared/shared.module';



@NgModule({
  declarations: [PreparationDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
  ]
})
export class PrepartionDetailModule { }
