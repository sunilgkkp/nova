import { PreparationDetailComponent } from './preparation-detail/preparation-detail.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PreparationPageRoutingModule } from './preparation-routing.module';

import { PreparationPage } from './preparation.page';
import { SharedModule } from 'src/app/shared/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    PreparationPageRoutingModule
  ],
  declarations: [PreparationPage],
  exports: [PreparationPage]
})
export class PreparationPageModule { }
