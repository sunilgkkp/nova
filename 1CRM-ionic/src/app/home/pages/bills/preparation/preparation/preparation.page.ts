import { ModalController } from '@ionic/angular';
import { PreparationDetailsPage } from './../preparation-details/preparation-details.page';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IApiUser } from 'src/controller/apiAppData/apiUser/user-data.service';
import { BillService } from 'src/controller/services/billService/bill.service';
import { FunctionService } from 'src/controller/services/function/function.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';
import { ProfileService } from 'src/controller/services/profile/profile.service';
import { Collections } from 'src/controller/utils/Collections';
import { PreparationDetailComponent } from 'src/app/home/pages/bills/preparation/preparation/preparation-detail/preparation-detail.component';

@Component({
  selector: 'app-preparation',
  templateUrl: './preparation.page.html',
  styleUrls: ['./preparation.page.scss'],
})
export class PreparationPage implements OnInit {

  public loading: any;
  public profile: IApiUser;
  public items: any = [];
  public moreItems: any = [];
  public currentPage: number = 1;
  constructor(
    private translate: TranslateService,
    private billsService: BillService,
    private navCtrl: NavigationService,
    private profileService: ProfileService,
    public fun: FunctionService,
    public modalCtrl: ModalController
  ) { }

  ngOnInit() {
    this.profileService.getCurrentUser().then((user) => {
      this.profile = user;
      this.fetch([], 10, this.currentPage);
    });
  }

  fetch(loaded?: any[], load: number = 10, page = 1): any {
    if (page == 1) {
      this.items = [];
      this.moreItems = [];
    }
    this.fun.presentLoading('common.fetching-data').then(() => {
      return this.billsService.assignments(this.profile.roleId(), loaded && loaded.length, load, page).then((items) => {
        this.setItems(loaded, items);
      }).catch((error) => {
        console.log('error', error);
      });
    }).catch((error) => {
      console.log('error', error);
    });
  }

  protected setItems(current: any[], items: any, property: string = 'id') {
    this.items = Collections.unique((current ? this.items : []).concat(items.data || items), property);
    this.moreItems = !items.meta || this.items.length < items.meta.pagination.total;
  }

  async showDetails(assignment: any) {
    let d: any = { assignment, profile: this.profile }
    const modal = await this.modalCtrl.create({
      component: PreparationDetailComponent,
      cssClass: 'full',
      componentProps: d
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.doRefresh();
    }
  }


  doRefresh(event?: any) {
    if (event !== undefined) {
      setTimeout(() => {
        event.target.complete();
        this.currentPage = 1;
        this.fetch([], 10, this.currentPage);
      }, 2000);
    }
  }

  onChange(job: any) {
    this.doRefresh();
  }

  loadData(event, loaded, load) {
    setTimeout(() => {
      event.target.complete();
      this.currentPage = this.currentPage + 1;
      this.fetch(loaded, load, this.currentPage);
    }, 500);
  }

}
