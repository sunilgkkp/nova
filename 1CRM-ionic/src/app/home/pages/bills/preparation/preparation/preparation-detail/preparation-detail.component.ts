import { SurveyPage } from '../../../../survey/survey.page';
import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { IAssignmentDetails, IDocument, IReportSet } from 'src/app/home/pages/bills/bills/bills.module';
import { RevenuReportDetailPage } from 'src/app/home/pages/bills/preparation/revenu-report-detail/revenu-report-detail.page';
import { ISurvey } from 'src/app/home/pages/survey/survey.module';
import { IApiUser } from 'src/controller/apiAppData/apiUser/user-data.service';
import { BillService } from 'src/controller/services/billService/bill.service';
import { GeneralService } from 'src/controller/services/general/general.service';
import { Collections } from 'src/controller/utils/Collections';

interface ISurveyStatus {
  available?: boolean;
  answered?: boolean;
}

@Component({
  selector: 'app-preparation-detail',
  templateUrl: './preparation-detail.component.html',
  styleUrls: ['./preparation-detail.component.scss'],
})
export class PreparationDetailComponent implements OnInit {

  processing: boolean;
  details: IAssignmentDetails;
  profile: IApiUser;
  previous: { reports: IReportSet };
  questionnaire: ISurveyStatus = {};
  feedback: ISurveyStatus = {};

  constructor(
    private params: NavParams,
    private billsService: BillService,
    private generalService: GeneralService,
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
    this.details = this.params.get('assignment');
    this.profile = this.params.get('profile');
    if (this.details != undefined) {
      const req: any = this.details.fetched && this.details || this.billsService.assignment(this.profile.roleId(), this.details.id);
      Promise.resolve(req).then((assignment) => {
        this.details = assignment;
        this.details.fetched = true;
        this.previous = Collections.copy({ reports: this.details.reports });
        this.questionnaire.available = Boolean(this.details.questionnaire && this.details.questionnaire.questions &&
          this.details.questionnaire.questions.length);
        this.questionnaire.answered = this.questionnaire.available && Boolean(this.details.questionnaire.instance);
        this.feedback.available = Boolean(this.details.feedback && this.details.feedback.questions && this.details.feedback.questions.length);
        this.feedback.answered = this.feedback.available && Boolean(this.details.feedback.instance);
      });
    }
  }

  reportFillable(): boolean {
    return this.details.fetched && (!this.questionnaire.available || this.questionnaire.answered) && (!this.feedback.available || this.feedback.answered);
  }

  onReportUpload(document: IDocument, type: string) {
    this.generalService.confirmAlert({
      context: 'bills.preparation.details.' + type,
      header: 'confirm.title',
      message: 'confirm.message',
      item: document,
      confirm: true,
      cancel: true,
      onConfirm: () => {
        this.details.reports[type] = document;
        return this.onDocumentsSubmit(type);
      },
    });
  }

  onDocumentsSubmit(type) {
    this.processing = true;
    return this.billsService.submitDocuments(this.details.id, type, this.details.reports, this.previous.reports).then(
      () => {
        this.previous = Collections.copy({ reports: this.details.reports });
        return this.generalService.notifyPresent('bills.preparation.details.' + type + (this.details.reports[type] ? '.submit' : '.remove') + '.success');
      }
    ).finally(() => this.processing = false);
  }


  async editRevenue() {
    let d: any = { assignment: this.details }
    const modal = await this.modalCtrl.create({
      component: RevenuReportDetailPage,
      cssClass: 'full',
      componentProps: d
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data) {
      this.details.revenue = data;
      const revenue = this.details.revenue && Object.assign(this.details.revenue, {
        freelancerId: this.profile.roleId(),
        job_id: this.details.job.id,
        assignment_ids: [this.details.id],
      });
      this.processing = true;
      this.billsService.submitRevenue(revenue).then(
        () => this.generalService.notifyPresent('bills.preparation.submit.revenue.success')
      ).finally(() => this.processing = false);
    }
  }

  async editSurvey(type: string) {
    let d: any = {
      instance: this.details[type],
      save: (data: ISurvey) => this.billsService.submitSurvey(data, this.details),
    }
    const modal = await this.modalCtrl.create({
      component: SurveyPage,
      cssClass: 'full',
      componentProps: d
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data) {
      this.details[type] = data;
      this[type].answered = true;
    }
  }

  onSubmit() {
    const revenue = this.details.revenue && Object.assign(this.details.revenue, {
      freelancerId: this.profile.roleId(),
      job_id: this.details.job.id,
      assignment_ids: [this.details.id],
    });
    this.processing = true;
    this.billsService.submit(this.details.id, this.details.reports, this.previous.reports, revenue).then(
      () => this.generalService.notifyPresent('bills.preparation.submit.success') && this.modalCtrl.dismiss(true),
      () => this.processing = false
    );
  }

  onRemove(document: IDocument, type: string) {
    this.generalService.confirmAlert({
      context: 'bills.preparation.details.' + type + '.remove',
      header: 'confirm.title',
      message: 'confirm.message',
      item: document,
      confirm: true,
      cancel: true,
      onConfirm: () => {
        this.details.reports[type] = document;
        return this.onDocumentsSubmit(type);
      },
    });
  }

  close() {
    this.modalCtrl.dismiss()
  }

}
