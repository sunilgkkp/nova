import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RevenuReportDetailPageRoutingModule } from './revenu-report-detail-routing.module';

import { RevenuReportDetailPage } from './revenu-report-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RevenuReportDetailPageRoutingModule
  ],
  declarations: [RevenuReportDetailPage]
})
export class RevenuReportDetailPageModule {}
