import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RevenuReportDetailPage } from './revenu-report-detail.page';

const routes: Routes = [
  {
    path: '',
    component: RevenuReportDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RevenuReportDetailPageRoutingModule {}
