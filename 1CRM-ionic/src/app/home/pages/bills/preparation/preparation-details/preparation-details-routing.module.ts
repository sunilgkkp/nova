import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreparationDetailsPage } from './preparation-details.page';

const routes: Routes = [
  {
    path: '',
    component: PreparationDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreparationDetailsPageRoutingModule {}
