
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PreparationDetailsPageRoutingModule } from './preparation-details-routing.module';

import { PreparationDetailsPage } from './preparation-details.page';
import { SharedModule } from 'src/app/shared/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    TranslateModule,
    PreparationDetailsPageRoutingModule
  ],
  declarations: [PreparationDetailsPage]
})
export class PreparationDetailsPageModule { }
