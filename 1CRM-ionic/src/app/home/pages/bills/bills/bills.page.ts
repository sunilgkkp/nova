import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';

@Component({
  selector: 'app-bills',
  templateUrl: './bills.page.html',
  styleUrls: ['./bills.page.scss'],
})
export class BillsPage implements OnInit {

  public selectedSegment: string = '';
  public type: string;
  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavigationService
  ) { }

  ngOnInit() {
    let getParam: string = 'preparation';
    this.route.params.subscribe((params: Params) => {
      getParam = params.id ? params.id : 'preparation';
    });
    this.selectedSegment = getParam
    this.type = getParam;
  }

  segmentChanged(event): void {
    this.selectedSegment = event.detail.value;
    if (this.selectedSegment) {
      this.navCtrl.goTo('/home/tabs/invoices');
    }
  }

}
