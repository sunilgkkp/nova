import { InvoicesPage } from './../invoices/invoices/invoices.page';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { BillsPageRoutingModule } from './bills-routing.module';
import { BillsPage } from './bills.page';
import { ISurvey } from '../../survey/survey.module';
import { SharedModule } from 'src/app/shared/shared/shared.module';
import { PreparationPage } from '../preparation/preparation/preparation.page';
import { CreateinvoicePage } from '../invoices/createinvoice/createinvoice.page';
import { FilterModule } from 'src/app/components/filter/filter.module';

export interface IDocument {
  id: number;
  type: string;
  assignments: { data: any[] };
  assignmentsIds?: number[];
  approval?: any;
  invoiced?: boolean;
}

export interface IRevenue {
  id?: number;
  assignment_ids?: number[];
  sales_volume: any[];
  changed?: boolean;
  remove?: boolean;
  sum?: string | number;
  total?: number;
}

export interface IAssignmentDetails extends IAssignment {
  templates: any;
  job?: IJob;
  reports?: IReportSet;
  saleslots?: any[];
  incentives?: Array<{ name: string, value: string, selected: boolean }>;
  freelancer?: { id: number };
  questionnaire?: ISurvey;
  feedback?: ISurvey;
  fetched?: boolean;
  start_at?: Date;
  end_at?: Date;
  costs_on_time?: any;
  costs: any;
}

export interface IReportSet {
  [key: string]: IDocument;
}

export interface IAssignmentsRange {
  dates: { start: Date, end: Date };
  rates: { min: number, max: number, sum: { min: number, max: number } };
}

export interface IAssignment {
  id: number;
  appointed_at: string | Date;
  start_time: string;
  finish_time: string;
  is_done: boolean;
  disabled: boolean;
  state: string;
  date?: any;
  documents?: { data: IDocument[] };
  revenues?: { data: IRevenue[] };
  invoices?: { data: Array<{ state: string }> };
  revenue?: IRevenue;
  invoice?: { state: string };
  incentive_model?: { data: any };
  additional_costs?: any[];
  min_estimated_costs?: string;
  max_estimated_costs?: string;
  wage?: number;
  questionnaire_id?: number;
  freelancer_assignment_questionnaire_instance_id?: number;
  has_invoice_requirements: boolean;
  is_prepareable?: boolean;
  contract_type?: {
    identifier: string;
  };
  vat?: string;
}

export interface IAssignmentsCount {
  all: number;
  done: number;
}

export interface IJobInfo {
  range: IAssignmentsRange;
  count: IAssignmentsCount;
  assignments: IAssignment[];
}

export interface IJob {
  id: number;
  title: string;
  site: { data: any[] };
  project: { data: { client: { data: any[] } } };
  dates: { data: Array<{ assignments: { data: IAssignment[] } }> };
  documents?: { data: IDocument[] };
  revenues?: { data: IRevenue[] };
  saleslots?: { data: any[] };
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    FilterModule,
    BillsPageRoutingModule
  ],
  declarations: [BillsPage, PreparationPage, CreateinvoicePage, InvoicesPage]
})
export class BillsPageModule { }
