import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';

@Component({
  selector: 'app-training-details',
  templateUrl: './training-details.page.html',
  styleUrls: ['./training-details.page.scss'],
})
export class TrainingDetailsPage implements OnInit {

  constructor(
    public navCtrl: NavigationService
  ) { }

  ngOnInit() {
  }

  back() {
    this.navCtrl.modalDissmis();
  }
}
