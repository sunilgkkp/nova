import { FilterModule } from './../../../../components/filter/filter.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CertificatesPageRoutingModule } from './certificates-routing.module';

import { CertificatesPage } from './certificates.page';
import { SharedModule } from 'src/app/shared/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    FilterModule,
    CertificatesPageRoutingModule
  ],
  declarations: [CertificatesPage]
})
export class CertificatesPageModule { }
