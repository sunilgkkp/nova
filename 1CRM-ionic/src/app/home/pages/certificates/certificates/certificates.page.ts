import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { LoadingController, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { IFilterBarItems } from 'src/app/components/filter/filter.component';
import { IApiUser } from 'src/controller/apiAppData/apiUser/user-data.service';
import { CertificatesService, IFilter } from 'src/controller/services/certificates/certificates.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';
import { ProfileService } from 'src/controller/services/profile/profile.service';
import { Collections } from 'src/controller/utils/Collections';
import { TrainingDetailsPage } from '../training-details/training-details.page';

@Component({
  selector: 'app-certificates',
  templateUrl: './certificates.page.html',
  styleUrls: ['./certificates.page.scss'],
})
export class CertificatesPage implements OnInit {

  public selectedSegment: string = '';
  public currentPage: number = 1;
  public items: any = [];
  public type: string;
  public filters: IFilterBarItems;
  public filtered: IFilter = {};
  public loading: any;
  public profile: IApiUser;
  filterOpened: boolean = true;
  public moreItems: any = [];
  constructor(
    private loadingCtrl: LoadingController,
    private translate: TranslateService,
    private profileService: ProfileService,
    private route: ActivatedRoute,
    private modalCtrl: ModalController,
    public navCtrl: NavigationService,
    private certificatesService: CertificatesService,
  ) { }

  ngOnInit() {
    let getParam: string = 'all';
    this.route.params.subscribe((params: Params) => {
      getParam = params.id ? params.id : 'all';
      delete params.pos
    });
    this.selectedSegment = getParam;
    this.type = getParam;
    this.profileService.getCurrentUser().then((user) => {
      this.profile = user;
      this.initFilters();
      this.fetch([], 10, this.currentPage);
    });
  }

  segmentChanged(event): void {
    this.selectedSegment = event.detail.value;
    this.currentPage = 1;
    this.fetch([], 10, this.currentPage);
    if (this.selectedSegment) {
      this.navCtrl.goTo('home/tabs/certificates/' + this.selectedSegment)
    }
  }

  initFilters() {
    this.filters = {
      search: { type: 'search', set: '' },
      recommendation: { type: 'buttons', options: 'certificates.recommendation', set: undefined },
      category: { type: 'select', options: 'certificates.categories', set: undefined, none: true }
    };
  }

  onFilterOpened(isOpen) {
    this.filterOpened = isOpen;
  }

  async presentLoading(text: string) {
    this.loading = await this.loadingCtrl.create({
      message: this.translate.instant(text),
      backdropDismiss: false,
    });

    return await this.loading.present();
  }

  fetch(loaded?: any[], load: number = 10, page = 1): any {
    if (page == 1) {
      this.items = [];
      this.moreItems = [];
    }
    this.presentLoading('common.fetching-data').then(() => {
      return this.certificatesService.list(this.selectedSegment, this.profile.roleId(), loaded && loaded.length, load, this.filtered, page).then((items) => {
        console.log('items => ', items);
        this.setItems(loaded, items);
        this.loading.dismiss();
      }).catch((error) => {
        console.log('error', error);
        this.loading.dismiss();
      });
    }).catch(() => {
      this.loading.dismiss();
    });
  }

  protected setItems(current: any[], items: any, property: string = 'id') {
    this.items = Collections.unique((current ? this.items : []).concat(items.data || items), property);
    this.moreItems = !items.meta || this.items.length < items.meta.pagination.total;
  }

  doRefresh(event?: any) {
    setTimeout(() => {
      event.target.complete();
      this.currentPage = 1;
      this.fetch([], 10, this.currentPage);
    }, 2000);
  }


  loadData(event, loaded, load) {
    setTimeout(() => {
      event.target.complete();
      this.currentPage = this.currentPage + 1;
      this.fetch(loaded, load, this.currentPage);
    }, 500);
  }

  filter(filtered: IFilter) {
    this.filtered = filtered;
    this.fetch([], 10, this.currentPage);
  }

  async showDetails(trainingId: any, passed) {
    let data: any = { id: trainingId, passed: passed }
    const modal = await this.modalCtrl.create({
      component: TrainingDetailsPage,
      cssClass: 'full',
      componentProps: data
    });
    return await modal.present();
  }

}
