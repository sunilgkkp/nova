import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { MessageService } from 'src/controller/services/messageService/message.service';

@Component({
  selector: 'app-create-message',
  templateUrl: './create-message.page.html',
  styleUrls: ['./create-message.page.scss'],
})
export class CreateMessagePage implements OnInit {

  editable: { subject: boolean, content: boolean } = { subject: true, content: true };
  message: any;
  header: any;
  data: any;
  type: string;
  processing: boolean;
  public messages: any;

  constructor(
    private modalCtrl: ModalController,
    private params: NavParams,
    private messageService: MessageService
  ) {
    this.messages = this.params.get('service');
    this.message = this.params.get('message') || {};
    this.header = this.params.get('header') || {};
    this.data = this.params.get('data');
    this.type = this.params.get('type');
    Object.assign(this.editable, this.params.get('editable') || {});
  }

  ngOnInit() {
  }

  send() {
    this.processing = true;
    this.messageService.submit(this.type, this.message.subject, this.message.content, this.data).then(() => {
      this.modalCtrl.dismiss('sent');
    }).finally(() => this.processing = false);
  }

}
