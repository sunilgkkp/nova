import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { GeneralService } from 'src/controller/services/general/general.service';
import Collection from 'src/controller/utils/Collection';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  settingPage: any;
  constructor(
    private auth: AuthenticationService,
    private generalService: GeneralService
  ) {
    this.settingPage = Collection.settingPages;
    console.log(this.settingPage);

  }

  ngOnInit() {
  }

  logout() {
    this.generalService.confirmAlert({
      header: 'auth.logout.confirm',
      cancel: true,
      confirm: true,
      onConfirm: () => {
        this.auth.logout()
      },
    });
  }

}
