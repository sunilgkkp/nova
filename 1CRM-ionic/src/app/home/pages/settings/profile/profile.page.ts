import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ProfileService } from 'src/controller/services/profile/profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  public profile: any;
  public loading: any;
  public userRating: number = 1;
  constructor(
    private profileService: ProfileService,
    private loadingCtrl: LoadingController,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
  }

  async presentLoading(text: string) {
    this.loading = await this.loadingCtrl.create({
      message: this.translate.instant(text),
      backdropDismiss: false,
    });

    return await this.loading.present();
  }

  ionViewWillEnter() {
    this.presentLoading('common.fetching-data').then(() => {
      this.profileService.getCurrentUser().then((user) => {
        return this.profileService.get(user.roleId()).then((profile) => {
          console.log(this.profile);
          this.profile = profile.data;
          this.userRating = profile.avg_assignment_rating;
          this.profile.email = user.email();
          this.loading.dismiss();
        });
      }).catch((error) => {
        this.loading.dismiss();
      });
    }).catch(() => {
      this.loading.dismiss();
    });
  }

  logRatingChange(ev) {
    console.log("logRatingChange = ", ev)
  }
}
