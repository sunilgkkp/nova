import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';

@Component({
  selector: 'app-imprint',
  templateUrl: './imprint.page.html',
  styleUrls: ['./imprint.page.scss'],
})
export class ImprintPage implements OnInit {

  constructor(
    public navCtrl: NavigationService
  ) { }

  ngOnInit() {
  }

  back() {
    this.navCtrl.goBack();
  }
}
