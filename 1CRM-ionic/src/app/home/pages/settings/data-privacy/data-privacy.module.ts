import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DataPrivacyPageRoutingModule } from './data-privacy-routing.module';

import { DataPrivacyPage } from './data-privacy.page';
import { SharedModule } from 'src/app/shared/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    DataPrivacyPageRoutingModule
  ],
  declarations: [DataPrivacyPage]
})
export class DataPrivacyPageModule { }
