import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SurveyPageRoutingModule } from './survey-routing.module';

import { SurveyPage } from './survey.page';
import { IApproval } from 'src/controller/services/file/file.service';

export const SurveyType = {
  Questionnaire: 'questionnaire',
  Feedback: 'feedback',
};

export interface ISurveyQuestion {
  question: string;
  type: string;
  answer?: string | boolean;
  comment?: string;
}

export interface ISurvey {
  id?: number;
  questions?: ISurveyQuestion[];
  instance?: number;
  type?: string;
  comment?: string;
  approval?: IApproval
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SurveyPageRoutingModule
  ],
  declarations: [SurveyPage]
})
export class SurveyPageModule { }
