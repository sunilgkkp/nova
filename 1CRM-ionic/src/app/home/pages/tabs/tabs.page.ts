import { EventsService } from '../../../../controller/services/events/events.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonTabs } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { NavigationService } from '../../../../controller/services/navigationRoute/navigation.service';
import Collection from '../../../../controller/utils/Collection';
@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  private indicies: any = {};
  tabItem: any;
  @ViewChild('tabs') tabs: IonTabs;

  constructor(
    private translate: TranslateService,
    public navCtrl: NavigationService,
    public events: EventsService
  ) {
    this.tabItem = Collection.pages
  }

  ngOnInit() {
    let i = 0;
    this.tabItem.forEach((tab) => {
      Object.assign(tab, {
        title: this.translate.instant(`${tab.name}.tab-name`),
      });
      if (tab.tab) {
        this.indicies[tab.name] = i;
        i++;
      }
    });
    this.events.getObservable().subscribe((event) => {
      if (event.name == 'tabChange') {
        if (event.data.params === 'all') {
          this.navCtrl.goTo(['home/tabs/' + event.data.tabName, event.data.params]);
        }
        else if (event.data.params === 'preparation') {
          this.navCtrl.goTo(['home/tabs/invoices/' + event.data.params]);
        }
        else {
          this.redirect(event.data.tabName, event.data.params)
        }
      }
    });
  }

  redirect(tabName = null, params = null) {
    this.navCtrl.goTo(['tabs/' + tabName, params]);
  }

}
