import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardPageModule)
      },
      {
        path: 'assignments',
        loadChildren: () => import('./pages/assignments/assignment-list/assignment-list.module').then(m => m.AssignmentListPageModule)
      },
      {
        path: 'jobposting',
        loadChildren: () => import('../jobs/jobs-list/jobs-list.module').then(m => m.JobsListPageModule)
      },
      {
        path: 'invoices',
        loadChildren: () => import('../bills/bills/bills.module').then(m => m.BillsPageModule)
      },
      {
        path: 'certificates',
        loadChildren: () => import('../certificates/certificates/certificates.module').then(m => m.CertificatesPageModule)
      },
      {
        path: 'settings',
        loadChildren: () => import('../settings/settings.module').then(m => m.SettingsPageModule)
      },
      {
        path: 'to-do',
        loadChildren: () => import('../to-do/to-do.module').then(m => m.ToDoPageModule)
      },
      {
        path: 'account-settings',
        loadChildren: () => import('../account-setting/account-setting.module').then(m => m.AccountSettingPageModule)
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule { }
