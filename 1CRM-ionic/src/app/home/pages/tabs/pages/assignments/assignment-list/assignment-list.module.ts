import { MessageCounterComponent } from '../../../../../../components/message-counter/message-counter.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { AssignmentListPageRoutingModule } from './assignment-list-routing.module';
import { AssignmentListPage } from './assignment-list.page';
import { AssignmentCardPage } from '../assignment-card/assignment-card.page';
import { TenderItemComponent } from 'src/app/components/tender-item/tender-item.component';
import { SharedModule } from 'src/app/shared/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    TranslateModule,
    AssignmentListPageRoutingModule
  ],
  declarations: [AssignmentListPage, AssignmentCardPage, MessageCounterComponent]
})
export class AssignmentListPageModule { }
