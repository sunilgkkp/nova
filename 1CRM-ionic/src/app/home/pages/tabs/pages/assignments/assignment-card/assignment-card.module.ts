import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AssignmentCardPageRoutingModule } from './assignment-card-routing.module';
import { TenderItemComponent } from '../../../../../../components/tender-item/tender-item.component';
import { AssignmentCardPage } from './assignment-card.page';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'src/app/shared/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    TranslateModule,
    AssignmentCardPageRoutingModule
  ],
  declarations: [AssignmentCardPage, TenderItemComponent]
})
export class AssignmentCardPageModule { }
