import { InvoiceDetailsPage } from './../../../../bills/invoices/invoice-details/invoice-details.page';
import { PreparationDetailComponent } from 'src/app/home/pages/bills/preparation/preparation/preparation-detail/preparation-detail.component';
import { BillService, IApiInvoice } from '../../../../../../../controller/services/billService/bill.service';
import { AppService } from '../../../../../../../controller/apiAppData/app.service';
import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AssignmentService, AssignmentState, IAssignmentStatus } from 'src/controller/services/assignment/assignment.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';
import { AssignmentDetailPage } from '../assignment-detail/assignment-detail.page';
import * as moment from 'moment';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { TranslateService } from '@ngx-translate/core';
import { ActionSheetController, Platform } from '@ionic/angular';
import { GeneralService } from '../../../../../../../controller/services/general/general.service';
import { FunctionService } from 'src/controller/services/function/function.service';
import { ProfileService } from 'src/controller/services/profile/profile.service';
import { IApiUser } from 'src/controller/apiAppData/apiUser/user-data.service';
import { IAssignmentDetails } from '../../../../bills/bills/bills.module';

@Component({
  selector: 'app-assignment-card',
  templateUrl: './assignment-card.page.html',
  styleUrls: ['./assignment-card.page.scss'],
})
export class AssignmentCardPage implements OnInit {

  @Input() assignment: any;
  @Output() refresh: EventEmitter<any> = new EventEmitter();
  status: IAssignmentStatus = {};

  public expanded: boolean;
  public timeout: any;
  public processing: boolean = false;

  private available: { whatsapp: boolean } = {
    whatsapp: false
  };

  constructor(
    private element: ElementRef,
    public navCtrl: NavigationService,
    private assignmentService: AssignmentService,
    private social: SocialSharing,
    private fun: FunctionService,
    public app: AppService,
    private billsService: BillService,
    private actionsCtrl: ActionSheetController,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    private generalService: GeneralService,
    public profileService: ProfileService,
  ) {

  }

  ngOnInit() { }

  ngAfterViewInit() {
    this.expanded = this.element.nativeElement.getAttribute('expanded') === 'true';
    this.cdr.detectChanges();
    this.assignment.siteName = this.assignment.site.name;
    this.recheckStatus();
  }

  showDetails() {
    this.navCtrl.popup(AssignmentDetailPage)
  }

  private recheckStatus(secs?: number) {
    this.timeout = setTimeout(() => {
      this.setStatus();
    }, secs * 1000);
  }

  private setStatus(recheck: boolean = true) {
    this.status = this.getStatus(this.assignment);
    if (this.expanded && recheck) {
      this.recheckStatus(10);
    }
  }

  getStatus(tender: any): IAssignmentStatus {
    const status = {
      header: 'today',
      button: 'check-in',
      state: AssignmentState.Coming,
      class: {
        today: moment().isSame(tender.start_at, 'day'),
        available: moment().isBetween(tender.checkin.available, tender.checkin.late),
        delayed: !tender.checkin.done && moment().isBetween(tender.start_at, tender.checkin.late),
        late: !tender.checkin.done && moment().isAfter(tender.checkin.late),
        checkedin: tender.checkin.done,
        checkedout: tender.checkout.done,
        prepared: tender.has_invoice_requirements,
        invoiced: tender.state === 'invoiced',
        done: tender.checkout.done && !tender.is_prepareable && tender.state !== 'invoiced',
      },
    };
    if (status.class.done) {
      status.header = 'done';
      status.button = 'done';
      status.state = AssignmentState.Done;
    } else if (status.class.prepared) {
      status.header = 'prepared';
      status.button = 'create-invoice';
      status.state = AssignmentState.Prepared;
    } else if (status.class.checkedout) {
      status.header = 'checkedout';
      status.button = 'upload-report';
      status.state = AssignmentState.CheckedOut;
    } else if (status.class.checkedin) {
      status.header = 'checkedin';
      status.button = 'check-out';
      status.state = AssignmentState.CheckedIn;
    } else if (status.class.available) {
      status.header = 'now';
      status.button = 'check-in';
      status.state = AssignmentState.Available;
    } else if (status.class.late) {
      status.header = 'late';
      status.button = 'late-check-in';
      status.state = AssignmentState.Late;
    }
    return status;
  }

  private trans(identifier: string, values?: any): string {
    return this.translate.instant('assignments.' + identifier, values);
  }

  action() {
    if (!this.processing) {
      this.processing = true;
      // return this.actions(this.assignment, this.status).then((e) => {
      // console.log('val of e => ', e);
      return this.update().then((res: any) => {
        this.fun.presentAlertConfirm('Check in successfully', ' Fill the survey ');
        this.processing = false;
        if (this.assignment.reported) {
          this.refresh.emit();
        }
      });
      // }).catch(() => {
      //   this.processing = false;
      // });
    }
  }

  actions(tender: any, status: IAssignmentStatus): Promise<any> {
    switch (status.state) {
      case AssignmentState.Done: {
        return Promise.resolve(true);
      }
      case AssignmentState.Available: {
        return this.checkIn(tender);
      }
      case AssignmentState.Late: {
        return this.lateCheckIn(tender);
      }
      case AssignmentState.CheckedIn: {
        return this.checkOut(tender);
      }
      case AssignmentState.CheckedOut: {
        return this.reportAssignment(tender);
      }
      case AssignmentState.Prepared: {
        return this.createInvoice();
      }
      case AssignmentState.Invoiced: {
        return this.invoiceDetails(tender);
      }
      default: {
        return Promise.reject(false);
      }
    }
  }

  checkIn(tender: any): Promise<any> {
    return new Promise((resolve, reject) =>
      this.generalService.confirmAlert({
        header: 'assignments.check-in.title',
        message: 'assignments.check-in.message',
        item: tender,
        confirm: true,
        cancel: true,
        onConfirm: () => {
          this.onCheckinConfirm(tender).then(resolve, reject)
        },
        onCancel: () => reject()
      })
    );
  }

  private onCheckinConfirm(tender: any): Promise<any> {
    return this.assignmentService.checkin(tender).then(() => 'checkedin')
  }

  lateCheckIn(tender: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const actionSheet: any = this.actionsCtrl.create({
        header: this.trans('late-check-in.title'),
        subHeader: this.trans('late-check-in.message', tender),
        buttons: [{
          text: this.trans('late-check-in.via-sms.button'),
          icon: 'text',
          handler: () => this.onShareSmsConfirm(tender, resolve, reject),
        }, {
          text: this.trans('late-check-in.via-whatsapp.button'),
          icon: 'whatsapp',
          cssClass: !this.available.whatsapp && 'disabled',
          handler: () => this.onShareWhatsappConfirm(tender, resolve, reject),
        }, {
          text: this.translate.instant('buttons.cancel'),
          role: 'cancel',
          handler: () => reject(),
        }],
      });

      actionSheet.present();
      const { role } = actionSheet.onDidDismiss();
      console.log('onDidDismiss resolved with role', role);
    });
  }

  private onShareSmsConfirm(tender: any, resolve, reject): void {
    const msg = this.message(tender, 'sms');
    const tel = tender.agent && tender.agent.mobile || '';
    this.social.shareViaSMS(msg, tel).then(() => resolve(false), reject);
  }

  private onShareWhatsappConfirm(tender: any, resolve, reject): void {
    const msg = this.message(tender, 'whatsapp');
    this.social.shareViaWhatsApp(msg).then(() => resolve(false), reject);
  }

  private message(tender: any, type: string) {
    const values = Object.assign(tender, {
      time: moment().format('HH:mm'),
      jobName: tender.job.title,
      agentName: tender.agent && tender.agent.fullname || '',
      freelancerName: `${tender.freelancer.firstname} ${tender.freelancer.lastname}`,
    });
    return this.trans(`late-check-in.via-${type}.message`, values);
  }

  checkOut(tender: any): Promise<any> {
    return new Promise((resolve, reject) =>
      this.generalService.confirmAlert({
        header: 'assignments.check-out.title',
        message: 'assignments.check-out.message',
        item: tender,
        confirm: true,
        cancel: true,
        onConfirm: () => {
          this.onCheckoutConfirm(tender).then(resolve, reject)
        },
        onCancel: () => reject()
      })
    );
  }

  private onCheckoutConfirm(tender: any): Promise<any> {
    return this.assignmentService.checkin(tender).then(() => 'checkedout');
  }

  async presentLoading(text: string) {
    this.translate.instant(text);
  }

  async reportAssignment(tender: any): Promise<any> {
    return new Promise((resolve) => {
      this.presentLoading('common.fetching-data').then(() => {
        return this.profileService.getCurrentUser().then((profile: IApiUser) => {
          return this.billsService.assignment(profile.roleId(), tender.assignment.id).then((assignment: IAssignmentDetails) => {
            assignment.fetched = true;
            let d: any = { profile, assignment }
            const modal: any = this.navCtrl.popup(PreparationDetailComponent)
            const { data } = modal.onWillDismiss();
            if (data) {
              resolve(data)
            }
          }).catch((error) => {
            console.log('error', error);
          });
        });
      }).catch((err) => {
        console.log('err .. ', err);
      });
    });
  }

  createInvoice(): Promise<any> {
    return
  }

  invoiceDetails(tender: any): Promise<any> {
    return new Promise((resolve) => {
      this.presentLoading('common.fetching-data').then(() => {
        return this.profileService.getCurrentUser().then((profile: IApiUser) => {
          return this.billsService.getByAssignment(profile.roleId(), tender.assignment.id).then((invoice: IApiInvoice) => {
            invoice.fetched = true;
            let d: any = { profile, invoice }
            const modal: any = this.navCtrl.popup(InvoiceDetailsPage);
            const { data } = modal.onWillDismiss();
            if (data) {
              resolve(data)
            }
          }).catch((error) => {
            console.log('error', error);
          });
        });
      }).catch((err) => {
        console.log(err);
      });
    });
  }

  private update(): Promise<any> {
    return this.assignmentService.get(this.assignment.freelancer.id, this.assignment.id, false).then((assignment) => {
      this.assignment = assignment;
      this.setStatus(false);
      return assignment;
    });
  }
}
