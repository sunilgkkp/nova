import { Collections } from '../../../../../../../controller/utils/Collections';
import { AppService } from '../../../../../../../controller/apiAppData/app.service';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FunctionService } from 'src/controller/services/function/function.service';
import { AssignmentService } from 'src/controller/services/assignment/assignment.service';
import { IApiUser } from 'src/controller/apiAppData/apiUser/user-data.service';
import { ProfileService } from 'src/controller/services/profile/profile.service';

@Component({
  selector: 'app-assignment-list',
  templateUrl: './assignment-list.page.html',
  styleUrls: ['./assignment-list.page.scss'],
})
export class AssignmentListPage implements OnInit {

  public items: any = [];
  public moreItems: any = [];
  public profile: IApiUser;
  public loading: any;
  public upcoming: boolean = true;
  public currentPage: number = 1;
  constructor(
    private translate: TranslateService,
    private app: AppService,
    public assignment: AssignmentService,
    private fun: FunctionService,
    private profileService: ProfileService,
  ) {
    this.translate.use('en');
  }

  ngOnInit() {
    this.profileService.getCurrentUser().then((user) => {
      this.profile = user;
      this.fetch([], 5, this.currentPage);
    });
  }

  async presentLoading(text: string) {
    this.translate.instant(text);
  }


  fetch(loaded?: any[], load: number = 5, page = 1): any {
    if (page == 1) {
      this.items = [];
      this.moreItems = [];
    }
    this.presentLoading('common.fetching-data').then(() => {
      return this.assignment.list(this.profile.roleId(), this.upcoming, loaded && loaded.length, load, page).then((items) => {
        this.setItems(loaded, items);
      }).catch((error) => {
        console.log('error', error);
      });
    }).catch((error) => {
      console.log('error', error);
    });
  }


  protected setItems(current: any[], items: any, property: string = 'id') {
    this.items = Collections.unique((current ? this.items : []).concat(items.data || items), property);
    this.moreItems = !items.meta || this.items.length < items.meta.pagination.total;
  }

  doRefresh(event?: any) {
    setTimeout(() => {
      event.target.complete();
      this.currentPage = 1;
      this.fetch([], 5, this.currentPage);
    }, 2000);
  }

  loadData(event, loaded, load) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();
      this.currentPage = this.currentPage + 1;
      this.fetch(loaded, load, this.currentPage);
    }, 500);
  }

}
