import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AssignmentCardPage } from './assignment-card.page';

const routes: Routes = [
  {
    path: '',
    component: AssignmentCardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssignmentCardPageRoutingModule {}
