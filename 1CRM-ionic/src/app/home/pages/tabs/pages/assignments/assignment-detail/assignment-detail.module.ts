import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { PipesModule } from '../../../../../../../controller/pipes/pipes/pipes.module';
import { NgPipesModule } from 'ngx-pipes';
import { Nl2BrPipeModule } from 'nl2br-pipe';

import { AssignmentDetailPageRoutingModule } from './assignment-detail-routing.module';

import { AssignmentDetailPage } from './assignment-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Nl2BrPipeModule,
    NgPipesModule,
    PipesModule,
    AssignmentDetailPageRoutingModule
  ],
  declarations: [AssignmentDetailPage]
})
export class AssignmentDetailPageModule { }
