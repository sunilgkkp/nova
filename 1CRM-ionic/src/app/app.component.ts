import { AppService } from '../controller/apiAppData/app.service';
import { Component, NgZone } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { App, URLOpenListenerEvent } from '@capacitor/app';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { StoreService } from 'src/controller/services/dbStore/store.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private translate: TranslateService,
    private app: AppService,
    private zone: NgZone,
    private platform: Platform,
    private router: Router,
    private store: StoreService
  ) {

    const selectLang = this.store.get('selectLanguage');

    if(selectLang != null) {
      this.translate.setDefaultLang(selectLang);
    } else {
      this.translate.setDefaultLang('en');
    }

    this.app.currentUser();

    this.platform.ready().then(() => {

      App.addListener('appStateChange', (data: any) => {
        console.log('[DEEP LINK ALT], ', data);
      });
      
      App.addListener('appUrlOpen', (event: URLOpenListenerEvent) => {
          this.zone.run(() => {
              console.log(event.url);
              const slug = event.url.split(".app").pop();
              console.log(slug);
              this.router.navigateByUrl('/');
          });
      });
    });
  }
}
