import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';
import { IonInput } from '@ionic/angular';
import { FunctionService } from 'src/controller/services/function/function.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  @ViewChild('password') passwordInput: IonInput;

  public loading: any;
  public credentials = { email: '', password: '' };
  public forgotPassword: any;
  public isShowPassword = false;

  constructor(
    private fun: FunctionService,
    private auth: AuthenticationService,
    public navCtrl: NavigationService,
  ) { }

  ngOnInit() { }

  showPassword() {
    this.passwordInput.type = this.passwordInput.type === 'password' ? 'text' : 'password';
    this.isShowPassword = !this.isShowPassword;
    this.passwordInput.setFocus();
  }

  gotoRegister() {
    this.navCtrl.goTo('auth/register', {}, 'forward');
  }

  forgotPasswordAction() {
    this.navCtrl.goTo('auth/forget-password', {}, 'forward');
  }

  login() {
    this.fun.presentLoading('authenticating').then(() => {
      this.auth.login(this.credentials);
    });
  }
}
