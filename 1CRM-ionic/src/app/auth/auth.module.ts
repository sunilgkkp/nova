import { ForgetComponent } from './forget/forget.component';
import { SelectLanguageComponent } from './select-language/select-language.component';
import { LoginComponent } from './login/login.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { RegisterComponent } from './register/register.component';
import { SharedModule } from '../shared/shared/shared.module';


@NgModule({
  declarations: [LoginComponent, SelectLanguageComponent, RegisterComponent, ForgetComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule,
    FormsModule,
    IonicModule,
  ]
})
export class AuthModule { }
