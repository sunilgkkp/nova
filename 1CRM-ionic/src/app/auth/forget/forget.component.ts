import { Component, Input, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { FunctionService } from 'src/controller/services/function/function.service';
import { GeneralService } from 'src/controller/services/general/general.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';

@Component({
    selector: 'app-forget',
    templateUrl: './forget.component.html',
    styleUrls: ['./forget.component.scss'],
})
export class ForgetComponent implements OnInit {

    public email: any;
    public loading: any;

    constructor(
        private auth: AuthenticationService,
        private generalService: GeneralService,
        private fun: FunctionService,
        private navCtrl: NavigationService
    ) { }

    ngOnInit() {
    }

    resetPassword(): void {
        const data = { email: this.email }
        this.fun.presentLoading('common.fetching-data').then(() => {
            this.auth.resetPassword(data).then((res: any) => {
                console.log(res);
            }).catch((status) => {
                this.notify(status?.error);
            });
        });
    }

    async notify(error?: any) {
    }
}
