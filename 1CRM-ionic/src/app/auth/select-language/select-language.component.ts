import { StoreService } from '../../../controller/services/dbStore/store.service';
import { GeneralService } from './../../../controller/services/general/general.service';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { appConfig } from 'src/controller/appConfig/app.config';
import { IonInput, MenuController, NavController, NavParams } from '@ionic/angular';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';
import { FunctionService } from 'src/controller/services/function/function.service';
import { AuthenticationService } from 'src/controller/auth/authentication.service';

@Component({
  selector: 'app-select-language',
  templateUrl: './select-language.component.html',
  styleUrls: ['./select-language.component.scss'],
})
export class SelectLanguageComponent implements OnInit {

  public selectLanguage;
  public loading: any;
  private isSelected = false;

  constructor(
    private translate: TranslateService,
    // private params: NavParams,
    // private generalService: GeneralService,
    private store: StoreService,
    public navCtrl: NavigationService,
    private fun: FunctionService,
    private auth: AuthenticationService,
  ) {
    this.selectLanguage = this.translate.defaultLang;
  }

  ngOnInit() {

  }

  // ionViewWillEnter() {
  //   if (this.params.data.reason) {
  //     setTimeout(() => {
  //       this.generalService.confirmAlert({ header: 'auth.error', message: this.params.data.reason, confirm: true });
  //       this.params.data.reason = '';
  //     });
  //   }
  // }

  changeOption(ev) {
    console.log('ev => ', ev.detail.value);
    // if (ev.detail.value == undefined) {
    //   this.generalService.notifyPresent("Please select langauge");
    //   return;
    // }
    const arryLan = ['en', 'de'];
    this.translate.addLangs(arryLan);
    this.selectLanguage = ev.detail.value;
    this.isSelected = true;
    this.store.store('selectLanguage', this.selectLanguage);
    this.translate.setDefaultLang(this.selectLanguage);
  }

  /////
  @ViewChild('password') passwordInput: IonInput;
  public credentials = { email: '', password: '' };
  public forgotPassword: any;
  public isShowPassword = false;

  showPassword() {
    this.passwordInput.type = this.passwordInput.type === 'password' ? 'text' : 'password';
    this.isShowPassword = !this.isShowPassword;
    this.passwordInput.setFocus();
  }

  gotoRegister() {
    this.navCtrl.goTo('auth/register', {}, 'forward');
  }

  forgotPasswordAction() {
    this.navCtrl.goTo('auth/forget-password', {}, 'forward');
  }

  login() {
    this.fun.presentLoading('authenticating').then(() => {
      this.auth.login(this.credentials);
    });
  }
}
