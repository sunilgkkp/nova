import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInput } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { FunctionService } from 'src/controller/services/function/function.service';
import { GeneralService } from 'src/controller/services/general/general.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';
import { StorageService } from 'src/controller/services/storage/storage.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {

  @ViewChild('password') passwordInput: IonInput;
  @ViewChild('password_confirmation') confirmpasswordInput: IonInput;

  public loading: any;
  public credentials = { email: '', password: '', email_confirmation: '' };
  public forgotPassword: any;
  public isShowPassword = false;
  public isShowConfirmPassword = false;

  constructor(
    private generalService: GeneralService,
    public fun: FunctionService,
    private auth: AuthenticationService,
    public translate: TranslateService,
    private navCtrl: NavigationService,
    private storageService: StorageService
  ) { }

  ngOnInit() { }

  showPassword(): void {
    this.passwordInput.type = this.passwordInput.type === 'password' ? 'text' : 'password';
    this.isShowPassword = !this.isShowPassword;
    this.passwordInput.setFocus();
  }

  showConfirmPassword(): void {
    this.confirmpasswordInput.type = this.confirmpasswordInput.type === 'password' ? 'text' : 'password';
    this.isShowConfirmPassword = !this.isShowConfirmPassword;
    this.confirmpasswordInput.setFocus();
  }

  gotoLogin() {
    this.navCtrl.goTo('auth');
  }

  register(): void {
    this.fun.presentLoading('authenticating').then(() => {
      this.auth.register(this.credentials);     
    });
  }

  private process(user) {
    this.storageService.setUserFirstName(user.data.firstname);
    this.navCtrl.goTo('home/tabs');
  }

  private showError(title: string, message: string): Promise<any> {
    return new Promise((resolve) => {
      const alert: any = this.generalService.confirmAlert({ header: title, message, confirm: true });
    });
  }

}
