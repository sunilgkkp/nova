import { PushNotifyToggleComponent } from './../../components/push-notify-toggle/push-notify-toggle.component';
import { TenderItemComponent } from './../../components/tender-item/tender-item.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgPipesModule } from 'ngx-pipes';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Nl2BrPipeModule } from 'nl2br-pipe';
import { NothingFoundComponent } from 'src/app/components/nothing-found/nothing-found.component';
import { InvoiceItemComponent } from 'src/app/components/invoice-item/invoice-item.component';
import { JobDetailComponent } from 'src/app/components/job-detail/job-detail.component';
import { BackButtonComponent } from 'src/app/components/back-button/back-button.component';
import { PipesModule } from 'src/controller/pipes/pipes/pipes.module';
import { AppliedDetailsComponent } from 'src/app/components/applied-details/applied-details.component';



const components: any = [
    NothingFoundComponent,
    TenderItemComponent,
    PushNotifyToggleComponent,
    BackButtonComponent,
    InvoiceItemComponent,
    JobDetailComponent,
    AppliedDetailsComponent
];

@NgModule({
    declarations: [components],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        Nl2BrPipeModule,
        NgPipesModule,
        TranslateModule,
        PipesModule
    ],
    exports: [components, TranslateModule],
    entryComponents: [components],
})
export class SharedModule { }
