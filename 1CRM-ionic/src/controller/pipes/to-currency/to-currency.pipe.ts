import { Format } from '../../utils/format';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toCurrency'
})
export class ToCurrencyPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    const retNumber = Number(value);
    return isNaN(retNumber) ? value : Format.numbers(value) + '€' || '';
  }

}
