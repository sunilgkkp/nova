import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilesizePipe } from '../filesize/filesize.pipe';
import { ToCurrencyPipe } from '../to-currency/to-currency.pipe';
import { DateFormatPipe } from '../dateFormat.pipe';


@NgModule({
  declarations: [FilesizePipe, ToCurrencyPipe, DateFormatPipe],
  imports: [
    CommonModule
  ],
  exports: [
    FilesizePipe,
    ToCurrencyPipe,
    DateFormatPipe
  ],
})
export class PipesModule { }
