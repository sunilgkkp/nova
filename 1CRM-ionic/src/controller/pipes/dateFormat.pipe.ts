import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
   name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform {
   transform(time: any): string {
      return moment(time, "HH:mm:ss").format("HH:mm");;
   }
}