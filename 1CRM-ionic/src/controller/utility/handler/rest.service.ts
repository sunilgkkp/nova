import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { appConfig } from 'src/controller/appConfig/app.config';

export declare interface IApiErrorResponse {
    status: number;
    message?: string;
}

@Injectable({
    providedIn: 'root'
})
export class RestService {

    baseUrl = appConfig.apiBaseUrl;

    constructor(private http: HttpClient,

    ) {
    }

    async post(url: string, body: any, contentType = 'application/json'): Promise<any> {
        return this.http.post(this.baseUrl + url, body, this.getHeaders(contentType)).toPromise()
            .then((response: any) => {

                if (response && response.status == false) {
                    throw new Error(response.message)
                }
                return Promise.resolve(response)
            }).catch((e) => {

                return Promise.reject(e)
            })

    }

    async get(url: string, contentType = 'application/json'): Promise<any> {
        return this.http.get(this.baseUrl + url, this.getHeaders(contentType)).toPromise()
            .then((response: any) => {

                if (response && response.status == false) {
                    throw new Error(response.message)
                }
                return Promise.resolve(response);
            })
            .catch((e) => {

                return Promise.reject(e);
            })
    }

    put(url: string, contentType = 'application/json'): Promise<any> {
        return this.http.put(this.baseUrl + url, this.getHeaders(contentType)).toPromise();
    }

    private getHeaders(contentType: string, opt = null): any {
        return {
            headers: new HttpHeaders({
                'Content-Type': contentType,
                'Accept': 'application/json, text/plain, */*'
            }),
        };
    }

    path(url: string, data?: any, remove: boolean = true): string {
        const params = url.match(/{\w+}/g);

        if (params && !data) {
            console.error('Data required for path ' + url); // tslint:disable-line no-console
            return;
        }

        (params || []).forEach((param) => {
            const property = param.substring(1, param.length - 1);
            let value = data[property];

            if (value === undefined) {
                console.error('Property ' + property + ' missing for path ' + url); // tslint:disable-line no-console
                value = '';
            }
            url = url.replace(param, value);
            if (remove) {
                delete data[property];
            }
        });
        return url;
    }
}
