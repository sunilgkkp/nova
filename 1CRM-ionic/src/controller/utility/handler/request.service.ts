/* eslint-disable guard-for-in */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import API from 'src/controller/apiAppData/data/api';
import { SpinnerService } from '../ui/spinner.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  requestor = 'http';
  constructor(
    private http: HttpClient,
    private spinner: SpinnerService,
    private toastr: ToastrService
  ) { }

  async send(requestName: any, data = {}, successHandler = null, errorHandler = null, backgroundmode = false, loaderHandler = 'spinner') {
    let loader = null;
    const url = () => {
      let tempurl = '';
      if (typeof API.endpoints[requestName].domain == 'undefined') {
        tempurl = API.defaultDomain + API.endpoints[requestName].url;
      } else {
        tempurl = API.endpoints[requestName].domain + API.endpoints[requestName].url;
      }

     
      for (const key in data) {
        const varible = '{{' + key + '}}';
        tempurl = tempurl.replace(varible, data[key]);
      }
  
      return tempurl;
    };

    if (backgroundmode === false) {
      loader = await this[loaderHandler].start();
    }

    const closeLoader = () => {
      if (loaderHandler === 'spinner') {
        if (loader !== null) {
          this[loaderHandler].stop(loader);
        }
      }
    };

    const errorHandlerDefault = (error) => {
      if (error.ok !== undefined && error.ok === false) {
        this.toastr.error(Object.values(error.error.errors).join(''));
      }
      else {
        this.toastr.error('Something went wrong');
      }
    };

    if (API.endpoints[requestName].requestType === 'post' || API.endpoints[requestName].requestType === 'put') {
      const requestor: HttpClient = this[this.requestor];
      // const headers = new HttpHeaders()
      // .set('content-type', 'application/json')
      // .set('Access-Control-Allow-Origin', 'application/json')
      // .set('Accept', '*/*');

      const subscriber = requestor[API.endpoints[requestName].requestType](url(), data, // {headers}
      ).subscribe((value) => {

        subscriber.unsubscribe();
        closeLoader();
        successHandler(value);

      }, (error) => {

        subscriber.unsubscribe();
        closeLoader();

        if (errorHandler === null) {
          errorHandlerDefault(error);
        }
      }, () => {
        console.log('final');
        closeLoader();
      });
    }

    if (API.endpoints[requestName].requestType === 'delete') {
      const requestor: HttpClient = this[this.requestor];
      // const headers = new HttpHeaders()
      // .set('content-type', 'application/json')
      // .set('Access-Control-Allow-Origin', 'application/json')
      // .set('Accept', '*/*');

      const subscriber = requestor.delete(url(), { body: data }
      ).subscribe((value) => {

        subscriber.unsubscribe();
        closeLoader();
        successHandler(value);

      }, (error) => {

        subscriber.unsubscribe();
        closeLoader();

        if (errorHandler === null) {
          errorHandlerDefault(error);
        }
      }, () => {
        console.log('final');
        closeLoader();
      });
    }

    if (API.endpoints[requestName].requestType === 'get') {
      const subscriber = this[this.requestor].get(url()).subscribe((value) => {

        subscriber.unsubscribe();
        closeLoader();
        successHandler(value);

      }, (error) => {

        subscriber.unsubscribe();
        closeLoader();

        if (errorHandler === null) {
          errorHandlerDefault(error);
        }
      }, () => {
        console.log('final');
        closeLoader();
      });
    }
  }
}
