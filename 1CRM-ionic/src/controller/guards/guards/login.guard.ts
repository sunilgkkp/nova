import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { NavigationService } from 'src/controller/services/navigationRoute/navigation.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanLoad {
  constructor(public auth: AuthenticationService, private router: NavigationService) { }
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.auth.isLoggedIn().then((loggedIn) => {
      if (!loggedIn) {
        this.router.goTo(['auth/login']);
        return false;
      }
      return true;
    });
  }
}
