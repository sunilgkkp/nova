import { SpinnerService } from './../utility/ui/spinner.service';
import {
   HttpErrorResponse,
   HttpEvent,
   HttpHandler,
   HttpInterceptor,
   HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { throwError } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { NavigationService } from '../services/navigationRoute/navigation.service';
import { AuthenticationService } from './authentication.service';
import { FunctionService } from '../services/function/function.service';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
   constructor(
      private router: NavigationService,
      private auth: AuthenticationService,
      public fun: FunctionService,
      private toastr: ToastrService,
      private spinner: SpinnerService,
   ) { }

   private handleAuthError(err: HttpErrorResponse): Observable<any> {
      if (err.status === 401 || err.status === 403) {
         this.auth.logout();
         this.router.goTo(['/auth/select-language']);
         if (err.error.message !== 'Wrong number of segments') {
            this.toastr.error(err.error.message);
         }
         this.spinner.stopAll();
         return of(err.message);
      }
      // else if (err.status == 422) {
      //    this.fun.showError('errors.login-failed', /^[a-z_]+$/.test(err.message) && 'errors.' + err.message || err.message);
      // }
      // else {
      //    this.fun.showError('errors.login-failed', /^[a-z_]+$/.test('Something went wrong') && 'errors.');
      // }
      return throwError(err);
   }

   intercept(
      req: HttpRequest<any>,
      next: HttpHandler
   ): Observable<HttpEvent<any>> {
      return from(Promise.resolve(localStorage.getItem('token'))).pipe(
         switchMap((token) => {
            const headers = req.headers.set('Authorization', `Bearer ${token}`);
            const requestClone = req.clone({ headers });
            return next
               .handle(requestClone)
               .pipe(catchError((err) => this.handleAuthError(err)));
         })
      );
   }
}