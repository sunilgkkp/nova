import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { RequestService } from '../utility/handler/request.service';
import { FunctionService } from '../services/function/function.service';
import { NavigationService } from '../services/navigationRoute/navigation.service';
import { RestService } from '../utility/handler/rest.service';
import { AppService } from '../apiAppData/app.service';
import { ProfileMainService } from '../services/profileServ/profile-main.service';
import { GeneralService } from '../services/general/general.service';
import { StorageService } from '../services/storage/storage.service';

const HAS_LOGGED_IN: any = 'hasLoggedIn';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  authState = new BehaviorSubject(false);
  constructor(
    public toastr: ToastrService,
    private request: RequestService,
    private app: AppService,
    public generalService: GeneralService,
    public profileService: ProfileMainService,
    public fun: FunctionService,
    private navCtrl: NavigationService,
    private storageService: StorageService,
    public platform: Platform,
    public restService: RestService
  ) {
    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });
  }

  ifLoggedIn() {
    const response = localStorage.getItem(HAS_LOGGED_IN);
    if (response === 'true') {
      this.authState.next(true);
    }
    return response;
  }

  getToken(): Promise<string> {
    return Promise.resolve(localStorage.getItem('token'));
  }

  isLoggedIn(): Promise<boolean> {
    return Promise.resolve(localStorage.getItem(HAS_LOGGED_IN) === 'true');
  }

  async login(credentials: any): Promise<boolean | any> {
    const success = async (value) => {
      if (value) {
        this.setLoggedIn(value.token);
        this.app.currentUser().then((res: any) => {
          this.storageService.setUserFirstName(res.data.firstname);
          console.log(res);

          if (res.data.status == 'active') {
            this.navCtrl.goTo('/home')
          }
          else if (res.data.status == 'onboarding') {
            this.navCtrl.goTo('/profile')
          }
          else if (res.data.status == "unconfirmed") {
            this.navCtrl.goTo('/profile')
          }
          else {
            this.toastr.error('Something went wrong');
          }
        });
        Promise.resolve(value);
      }
      else {
        console.log(value);
      }
    };
    this.request.send('login', credentials, success);
  }

  async register(data: any) {
    return new Promise((resolve, reject) => {
      const success = async (value) => {
        if (value) {
          this.generalService.notifyPresent('Successfully Register').then((res: any) => {
            this.navCtrl.goTo('/profile/privacy-policy');
          });
          resolve(value);
        }
      };
      this.request.send('register', data, success);
    })
  }

  logout() {
    localStorage.removeItem('selectLanguage');
    localStorage.removeItem('token');
    localStorage.removeItem('hasLoggedIn');
    localStorage.removeItem('user_profile');
    this.fun.closeMenu();
    this.navCtrl.goTo(['/auth/select-language']);
  }

  setLoggedIn(token = '') {
    return new Promise((resolve) => {
      localStorage.setItem('token', token);
      localStorage.setItem(HAS_LOGGED_IN, 'true');
      this.authState.next(true);
      resolve(true);
    });
  }

  async resetPassword(email: any): Promise<boolean | any> {
    try {
      const data: any = await this.restService.post('/auth/reset-password', email);
      return Promise.resolve(data);
    } catch (e) {
      return Promise.reject(e);
    }
  }

}
