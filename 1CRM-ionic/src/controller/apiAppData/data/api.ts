import { appConfig } from '../../appConfig/app.config';

const api = {
   endpoints: {
      login: {
         requestType: 'post',
         url: '/auth/login',
         responseInterface: [],
      },
      register: {
         requestType: 'post',
         url: '/auth/register',
         responseInterface: [],
      },
      qualifications: {
         requestType: 'post',
         url: '/qualifications',
         responseInterface: [],
      },
      addEmployee: {
         requestType: 'post',
         url: '/work-histories',
         responseInterface: [],
      },
      addTraining: {
         requestType: 'post',
         url: '/training-and-certificates',
         responseInterface: [],
      },
      currentUser: {
         requestType: 'get',
         url: '/users/current',
         responseInterface: [],
      },
      getAssignments: {
         requestType: 'get',
         url: '/freelancers/{{freelancerId}}/assignments?only_upcoming={{upcoming}}&include={{include}}&order_by={{order_by}}&&order_dir={{order_dir}}&page={{page}}&limit={{limit}}',
         responseInterface: [],
      },
      jobsAdvertisement: {
         requestType: 'get',
         url: '/job-advertisements?include={{include}}&limit={{limit}}&is_match=true&only_recommended=true',
         responseInterface: [],
      },
      getAllCertificates: {
         requestType: 'get',
         url: '/certificates',
         responseInterface: [],
      },
      getAllContractTypes: {
         requestType: 'get',
         url: '/contract-types',
         responseInterface: [],
      },
      getApprovals: {
         requestType: 'get',
         url: '/freelancers/{{freelancer_id}}/approvals',
         responseInterface: [],
      },
      freeLancers: {
         requestType: 'post',
         url: '/freelancers/{{freelancer_id}}',
         responseInterface: [],
      },
      freeLancersApproval: {
         requestType: 'post',
         url: '/freelancers/{{freelancer_id}}/approvals/master',
         responseInterface: [],
      },
      getJobs: {
         requestType: 'get',
         url: '/freelancers/{{freelancer_id}}/jobs?include=site,project.client.contacts,contract_type&limit=3&page=1&is_not_matching=true',
         responseInterface: [],
      },
      getJobsMatching: {
         requestType: 'get',
         url: '/freelancers/{{freelancer_id}}/jobs?include=site,project.client.contacts,contract_type&limit=3&is_matching=true',
         responseInterface: [],
      },
      getCertificates: {
         requestType: 'get',
         url: '/freelancers/{{freelancer_id}}/certificates?limit=3&page=1&without_passed=true&order_by=is_recommended&order_dir=desc&only_exclusive=true&include=assignments.date.job',
         responseInterface: [],
      },
      getAssignment: {
         requestType: 'get',
         url: '/freelancers/{{freelancer_id}}/assignments?include=date.job.site,freelancers.user,date.job.project.client,checkins.creator,checkins.updator,documents,revenues,invoices&limit=3&page=1&order_by=appointed_at&order_dir=asc&only_upcoming=true',
         responseInterface: [],
      },
      getAllInclude: {
         requestType: 'get',
         url: '/freelancers/{{freelancer_id}}?include=user,pictures,documents,references.document,qualifications.document,requests,certificates,ratings.creator,approvals,gtcs.documents,contract_types,trainingAndCertificates,identityDocuments,workHistories,legalDocuments,resume.document',
         responseInterface: [],
      },
      jobInvite: {
         requestType: 'get',
         url: '/job-invites',
         responseInterface: [],
      },
   },
   defaultDomain: appConfig.apiBaseUrl
}
export default api;