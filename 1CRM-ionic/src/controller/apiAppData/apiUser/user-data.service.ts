import { AppService } from './../app.service';
import { Injectable } from '@angular/core';
import { NewApiService } from 'src/controller/services/newApi/new-api.service';
import { Collections } from 'src/controller/utils/Collections';
@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  private fetching: any;
  private instance: any;
  private data: any;
  constructor(
    private newApiService: NewApiService
  ) { }

  get(): IApiUser {
    this.fetching = this.newApiService.getCurrentUser().then((data) => {
      console.log('data', data);
      this.data = data;
      // this.instance = this.user();
      this.fetching = null;
      return this.instance;
    }, () => {
      this.fetching = null;
      return Promise.reject(false);
    });
    return this.fetching;
  }

  private hasAccess(resource: string) {
    switch (resource) {
      case 'assignments':
      case 'jobs':
      case 'certificates':
      case 'bills': {
        return this.data && this.data.status !== 'onboarding';
      }
      default: {
        return true;
      }
    }
  }


  private isRestricted(identifier: string) {
    return (this.data.legal_blocked && this.data.legal_blocked[identifier] && 'legal-blocked')
      || (this.data.gtc_blocked && this.data.gtc_blocked[identifier] && 'gtc_blocked')
      || (this.data.contract_type_pending && this.data.contract_type_pending[identifier] && 'pending');
  }

  user(user): IApiUser {
    const data = Collections.copy(user.data);
    return {
      id: () => data.id,
      roleId: () => data.role_id,
      name: () => data.firstname,
      email: () => data.email,
      isOnboarding: () => data.status === 'onboarding',
      isDeactivated: () => data.status === 'deactivated',
      isRestricted: (identifier: string) => this.isRestricted(identifier),
      data: () => data,
      hasAccess: (resource: string) => this.hasAccess(resource),
    };
  }
}

export interface IApiUser {
  id(): number;
  roleId(): number;
  name(): string;
  email(): string;
  isOnboarding(): boolean;
  isDeactivated(): boolean;
  isRestricted(identifier: string): string;
  data(): any;
  hasAccess(resource: string): boolean;
}

