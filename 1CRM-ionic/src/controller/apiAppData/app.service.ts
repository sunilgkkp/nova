import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { RequestService } from '../utility/handler/request.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  userProfile: any = {};
  constructor(
    public request: RequestService,
    public toast: ToastrService
  ) { }

  async currentUser() {
    return new Promise((resolve, reject) => {
      const success = (value) => {
        if (value) {
          this.userProfile = value.data;
          localStorage.setItem('user_profile', JSON.stringify(this.userProfile))
          resolve(value);
        }
        else {
          reject(value);
          this.toast.error(value.message);
        }
      };
      this.request.send('currentUser', {}, success);
    });
  }

  async getApprovals(freelancer_id: number) {
    const data = { freelancer_id }
    const success = (value) => {
      if (value) {
        // console.log('value => ', value);
      }
      else {
        this.toast.error(value.message);
      }
    };
    this.request.send('getApprovals', data, success);
  }

  async addEducation(data: any) {
    return new Promise((resolve, reject) => {
      const success = (value) => {
        if (value) {
          resolve(value);
        }
        else {
          reject(value.message)
          this.toast.error(value.message);
        }
      };
      this.request.send('qualifications', data, success);
    });
  }

  async addTraining(data: any) {
    return new Promise((resolve, reject) => {
      const success = (value) => {
        if (value) {
          resolve(value);
        }
        else {
          reject(value.message)
          this.toast.error(value.message);
        }
      };
      this.request.send('addTraining', data, success);
    });
  }

  async addEmployee(data: any) {
    return new Promise((resolve, reject) => {
      const success = (value) => {
        if (value) {
          console.log('value => ', value);
          resolve(value);
        }
        else {
          reject(value.message)
          this.toast.error(value.message);
        }
      };
      this.request.send('addEmployee', data, success);
    });
  }

  async freeLancers(dataAl: any, freelancer_id: number) {
    dataAl.freelancer_id = freelancer_id 
    return new Promise((resolve, reject) => {
      const success = (value) => {
        if (value) {
          this.toast.info('Data saved success');
          resolve(value);
        }
        else {
          reject(value)
          this.toast.error('Something Went Worng');
        }
      };
      this.request.send('freeLancers', dataAl, success);
    });
  }

  freeLancersApproval(state: any, freelancer_id: number){
    state.freelancer_id = freelancer_id 
    return new Promise((resolve, reject) => {
      const success = (value) => {
        if (value) {
          resolve(value);
        }
        else {
          console.log('Something went wrong');
        }
      };
      this.request.send('freeLancersApproval', state, success);
    });
  }


}
