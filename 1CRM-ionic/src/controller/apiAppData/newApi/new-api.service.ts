import { Injectable } from '@angular/core';
import { RestService } from 'src/controller/utility/handler/rest.service';

@Injectable({
  providedIn: 'root'
})
export class NewApiService {

  constructor(public restService: RestService) { }

  async registerDevice(params: any): Promise<boolean | any> {
    try {
      const data: any = await this.restService.post('/users/{id}/device/register', params);

      return Promise.resolve(data);
    } catch (e) {
      return Promise.reject(e);
    }
  }

  async unregisterDevice(params: any): Promise<boolean | any> {
    try {
      const data: any = await this.restService.post('/users/{id}/device/unregister', params);

      return Promise.resolve(data);
    } catch (e) {
      return Promise.reject(e);
    }
  }
}
