import { Injectable } from '@angular/core';
import { ISurvey } from 'src/app/home/pages/survey/survey.module';
import { Collections } from 'src/controller/utils/Collections';

@Injectable({
  providedIn: 'root'
})
export class SurveyService {

  constructor() { }

  transform(data: any, type: string) {
    if (data[type]) {
      const instance = `${type}_instance`;
      data[type] = {
        id: data[type].id,
        questions: data[instance] && data[instance].instance || data[type].questionnaire || data[type] || [],
        instance: data[instance] && data[instance].id,
        approval: data[instance] && data[instance].approval && data[instance].approval.data,
        type,
      };
    }
  }

  prepare(data: ISurvey) {
    return {
      id: data.instance,
      type: data.type,
      instance: (data.questions || []).map((item: any) => Collections.only(item, ['question', 'answer', 'type', 'comment']))
    };
  }
}
