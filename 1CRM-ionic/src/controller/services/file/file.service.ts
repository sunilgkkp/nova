import { Injectable } from '@angular/core';

export interface IApproval {
  updatedAt?: string;
  createdAt?: string;
  state: string;
  comment: string;
  creator?: { name: string };
  updator?: { name: string };
}

@Injectable({
  providedIn: 'root'
})

export class FileService {

  constructor() { }

}
