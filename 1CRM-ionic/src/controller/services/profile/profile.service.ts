import { Injectable } from '@angular/core';
import { IApiUser, UserDataService } from 'src/controller/apiAppData/apiUser/user-data.service';
import { RestService } from 'src/controller/utility/handler/rest.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private restService: RestService, private user: UserDataService) {
  }

  async getCurrentUser(): Promise<IApiUser> {
    try {
      const user = await this.restService.get('/users/current');
      let currentUser: IApiUser = this.user.user(user);
      return Promise.resolve(currentUser);
    } catch (e) {
      return Promise.reject(e);
    }
  }

  /**
   * Gets freelancer profile data
   *
   * @param freelancerId
   */
  get(freelancerId: number): Promise<any> {
    return this.restService.get('/freelancers/' + freelancerId).then((profile) => this.transform(profile));
  }

  private transform(profile: any) {
    const fields = ['address', 'addressaddition', 'zip', 'city', 'country', 'near_to_city'];
    const addresses = [{}, {}];

    addresses.forEach((addr, idx) => {
      fields.forEach((field) => {
        const key = field + ((idx && '2') || ''); // to reflect datamodel
        if (profile[key]) {
          addr[field] = profile[key];
        }
      });
    });

    return Object.assign(profile, {
      addresses: addresses.filter((i) => Object.getOwnPropertyNames(i).length),
    });
  }
}
