import { Injectable } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  willData: any;
  navtigationData = null;
  constructor(
    public navCtrl: NavController,
    private modalCtrl: ModalController,
  ) { }


  async popup(component, data = null, cssClass = '') {
    if (data != null) {
      this.navtigationData = data;
    }
    const modal = await this.modalCtrl.create({ component, cssClass });
    await modal.present();
  }

  goTo(url, data = null, direction: 'forward' | 'back' | 'root' = 'forward') {
    if (data != null) {
      this.navtigationData = data;
    }
    switch (direction) {
      case 'forward': {
        this.navCtrl.navigateForward(url);
        break;
      }
      case 'back': {
        this.navCtrl.navigateBack(url);
        break;
      }
      default: {
        this.navCtrl.navigateRoot(url);
        break;
      }
    }
  }

  modalDissmis() {
    this.modalCtrl.dismiss();
  }

  goBack() {
    this.navCtrl.back();
  }
}
