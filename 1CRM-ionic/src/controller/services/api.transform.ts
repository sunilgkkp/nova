import * as moment from 'moment';
import { apiFormats } from '../appConfig/app.config';


export class ApiTransform {

    static time(time: string): string {
        return moment.utc(time, apiFormats.prepare.time).local().format(apiFormats.transform.time);
    }

    static date(day: string): Date {
        return moment.utc(day).toDate();
    }

    static datetime(datetime: string | Date, hours?: string): Date {
        const add: { hours?: number, minutes?: number } = {};
        const local = moment.utc(datetime, apiFormats.prepare.datetime).toDate();
        if (hours) {
            const parts = hours.split(':');
            add.hours = +parts[0];
            add.minutes = +parts[1];
            return moment(local).hours(add.hours).minutes(add.minutes).toDate();
        }
        return local;
    }
}
