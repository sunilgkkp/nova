import { RequestService } from "src/controller/utility/handler/request.service";
import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: "root",
})
export class ProfileMainService {
  constructor(public toast: ToastrService, private request: RequestService) {}

  async getJobs(freelancer_id: number, backgroundmode = false) {
    const data = { freelancer_id };
    const success = (value) => {
      if (value) {
        console.log("getJobs => ", value);
      } else {
        this.toast.error(value.message);
      }
    };
    this.request.send("getJobs", data, success, null, backgroundmode);
  }

  async getJobsMatching(freelancer_id: number, backgroundmode = false) {
    const data = { freelancer_id };
    const success = (value) => {
      if (value) {
        console.log("getJobsMatching => ", value);
      } else {
        this.toast.error(value.message);
      }
    };
    this.request.send("getJobsMatching", data, success, null, backgroundmode);
  }

  async getCertificates(freelancer_id: number, backgroundmode = false) {
    const data = { freelancer_id };
    const success = (value) => {
      if (value) {
        console.log("getCertificates => ", value);
      } else {
        this.toast.error(value.message);
      }
    };
    this.request.send("getCertificates", data, success, null, backgroundmode);
  }

  async getAssignment(freelancer_id: number, backgroundmode = false) {
    const data = { freelancer_id };
    const success = (value) => {
      if (value) {
        console.log("getAssignment => ", value);
      } else {
        this.toast.error(value.message);
      }
    };
    this.request.send("getAssignment", data, success, null, backgroundmode);
  }

  async getAllInclude(freelancer_id: number) {
    const data = { freelancer_id };
    return new Promise((resolve, reject) => {
      const success = (value) => {
        if (value) {
          resolve(value)
        } else {
          this.toast.error(value.message);
        }
      };
      this.request.send("getAllInclude", data, success);
    });
  }

  getAllData(id) {
    // this.getJobs(id);
    // this.getJobsMatching(id);
    // this.getCertificates(id);
    // this.getAssignment(id);
    // this.getAllInclude(id);
  }
}
