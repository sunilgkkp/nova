import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IAssignment, IAssignmentDetails, IAssignmentsCount, IAssignmentsRange, IDocument, IJob, IJobInfo, IReportSet, IRevenue } from 'src/app/home/pages/bills/bills/bills.module';
import { ISurvey, SurveyType } from 'src/app/home/pages/survey/survey.module';
import { appConfig } from 'src/controller/appConfig/app.config';
import { Collections } from 'src/controller/utils/Collections';
import { ApiTransform as transform } from '../api.transform';
import { SurveyService } from '../survey/survey.service';
import { ApiPrepare as prepare } from '../api.prepare';
import { Format as format } from '../../utils/format';
import { IApiRequestOptions, IFilter } from '../certificates/certificates.service';
import { Observable } from 'rxjs';

type TApiServiceMethod = (data: any, options?: IApiRequestOptions) => Observable<Response>;

export interface IApiInvoice {
  id?: number;
  issued_at?: string;
  comment?: string;
  total?: string;
  number?: string;
  job?: any;
  assignments?: any[];
  document?: any;
  includes_taxes?: boolean | string;
  document_id?: number;
  with_discount?: boolean;
  assignment_ids?: number[];
  approval?: { id?: number; state?: string };
  [key: string]: any;
}

export interface IApiNotify {
  operation: string;
  success?: boolean;
  error?: boolean;
}

interface IAssignmentOperation {
  document_id: number;
  assignment_id: number;
  operation: string;
  type: string;
}

export interface IApiResourcesOperation {
  id?: any;
  operation?: string;
  remove?: boolean;
  [key: string]: any;
}

@Injectable({
  providedIn: 'root'
})
export class BillService {

  public baseUrl = appConfig.apiBaseUrl;
  constructor(
    private http: HttpClient,
    private surveyService: SurveyService,
  ) { }

  async assignments(freelancerId: number, offset: number, limit: number, page = 1): Promise<any> {
    const opts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json, text/plain, */*'
      }),
      params: new HttpParams()
        .set('order_by', 'appointed_at')
        .set('order_dir', 'asc')
        .set('only_prepareable', true)
        .set('include', 'date.job.site,date.job.project.client')
        .set('page', page)
        .set('limit', limit)
    };

    try {
      const data: any = await this.http.get(this.baseUrl + '/freelancers/' + freelancerId + '/assignments', opts).toPromise();
      return Promise.resolve({
        data: data.data.map(this.transform, this),
        meta: data.meta
      });
    } catch (e) {
      return Promise.reject(e);
    }
  }

  /**
    * Transforms assignment data - unifies to tender-card data
    *
    * @param assignment
    */
  private transform(assignment: IAssignment): any {
    const reports: IReportSet = {};
    const flat = Collections.flatify(assignment, ['date', 'freelancer', 'date.job', 'date.job.project', 'date.job.project.client',
      'date.job.site', 'date.job.saleslots', 'questionnaire', 'questionnaire_instance', 'feedback', 'feedback_instance',
      'offer.tender.daily_rate_min', 'offer.tender.daily_rate_max']);
    const templates: { report?: any } = {};
    const revenue = assignment.revenues && assignment.revenues.data[0];
    const documents = assignment.documents && assignment.documents.data.map((document) => setDocuments(document));
    const title = flat.job ? flat.job.title.split(' | ')[0] : '';
    const warning = assignment.state === 'invoiced' ? !assignment.has_invoice_requirements : undefined;
    const incentives = assignment.incentive_model && this.numbers(assignment.incentive_model);
    const costs = assignment.additional_costs && this.numbers(assignment.additional_costs);
    if (revenue) {
      revenue.sum = assignment.revenues.data[0].total;
    }
    this.surveyService.transform(flat, SurveyType.Questionnaire);
    this.surveyService.transform(flat, SurveyType.Feedback);
    return Object.assign(assignment, flat, { job: { title, id: flat.job.id } }, {
      title, documents, templates, reports, revenue, warning, incentives, costs
    }, {
      start_at: transform.datetime(flat.date.appointed_at, assignment.start_time),
      contract_type_identifier: assignment.contract_type && assignment.contract_type.identifier
    });

    function setDocuments(document: IDocument) {
      if (document.type === 'template-report') {
        templates.report = document;
      }
      if (document.type === 'report' || document.type === 'picture-documentation') {
        reports[document.type] = Object.assign(document, { approval: document.approval && document.approval.data });
      }
      return document;
    }
  }

  private numbers(set: any | any[]): any {
    const res: any = {};
    if (Array.isArray(set)) {
      set.forEach((item: { name: string; value: string }) => res[item.name] = item.value);
    } else {
      Object.keys(set).forEach((key) => res[key] = set[key]);
    }
    return res;
  }

  /**
    * Gets list of invoices for given freelancer
    *
    * @param freelancerId
    */
  async list(freelancerId: number, offset: number, limit: number, filtered: IFilter, page = 1): Promise<any> {
    const opts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json, text/plain, */*'
      }),
      params: new HttpParams()
        .set('include', 'document,assignments.date.job.site,creator,assignments.documents,approval')
        .set('order_by', 'issued_at')
        .set('order_dir', 'desc')
        .set('page', page)
        .set('limit', limit)
    };

    if (filtered && filtered.search) {
      opts.params = opts.params.append('search', filtered.search);
    }
    if (filtered && filtered.date_from) {
      opts.params = opts.params.append('date_from', prepare.datetime(filtered && filtered.dates && filtered.dates.start));
    }
    if (filtered && filtered.date_to) {
      opts.params = opts.params.append('date_to', prepare.datetime(filtered && filtered.dates && filtered.dates.end));
    }
    if (filtered && filtered.state) {
      opts.params = opts.params.append('date_to', filtered.state);
    }

    try {
      const data: any = await this.http.get(this.baseUrl + '/freelancers/' + freelancerId + '/invoices', opts).toPromise();
      return Promise.resolve({
        data: data.data.map((item: any) => this.invoiceTransform(item)),
        meta: data.meta
      });
    } catch (e) {
      return Promise.reject(e);
    }
  }

  /**
     * Gets list of jobs for given freelancer
     *
     * @param freelancerId
     * @param type Assignments type (done|invoiceable)
     */
  async jobs(freelancerId: number, type: string, offset: number, limit: number, page = 1): Promise<any> {
    const opts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json, text/plain, */*'
      }),
      params: new HttpParams()
        .set(`only_${type}`, true)
        .set('include', 'site,dates.assignments.incentive_model,project.client')
        .set('page', page)
        .set('limit', limit)
    };

    try {
      const data: any = await this.http.get(this.baseUrl + '/freelancers/' + freelancerId + '/jobs/assignments', opts).toPromise();
      console.log('data', data);
      return Promise.resolve({
        data: data.data.map((job: IJob) => this.transformJob(job)).sort(this.byStart),
        meta: data.meta
      });
    } catch (e) {
      return Promise.reject(e);
    }
  }

  submitRevenue(revenue: IRevenue): Promise<any> {
    if (revenue && revenue.changed) {
      return this.resourcesUpdate([revenue], 'FreelancerRevenue');
    } else {
      return Promise.resolve();
    }
  }

  resourcesUpdate(operations: IApiResourcesOperation[], endpoint: string, notify?: string | IApiNotify): Promise<any> {
    const requests: Array<Promise<any>> = [];
    const removed: Array<Promise<any>> = [];
    const methods: { [key: string]: TApiServiceMethod } = {
      create: this['create' + endpoint],
      update: this['update' + endpoint],
      remove: this['remove' + endpoint],
    };
    if (!operations.length) {
      return Promise.resolve();
    }
    return Promise.all(removed).then(() => Promise.all(requests));
  }

  submitSurvey(data: ISurvey, assignment: IAssignmentDetails): Promise<any> {
    const params = Object.assign(this.surveyService.prepare(data), {
      freelancerId: assignment.freelancer.id,
      assignmentId: assignment.id
    });

    return this.resourcesUpdate([params], 'FreelancerAssignmentSurveyInstance', `bills.preparation.details.${data.type}.submit`).then(
      (response) => {
        if (data.type === 'feedback' && data.approval) {
          const approval = Object.assign(data.approval, { state: 'pending' });
          return this.resourcesUpdate([approval], 'SurveyApproval').then(
            () => Object.assign(data, { instance: response[0].id })
          );
        } else {
          return Object.assign(data, { instance: response[0].id });
        }
      });
  }

  /**
     * Submits assignment related documents
     *
     * @param id
     * @param report Report document
     * @param previous Previous report document
     * @param revenue
     */
  submitDocuments(id: number, type: string, reports: IReportSet, previous?: IReportSet): Promise<any> {
    const operations: IAssignmentOperation[] = [];
    const prevId = previous[type] && previous[type].id;
    const currId = reports[type] && reports[type].id;
    if (prevId && currId !== prevId) {
      this.addOperation(operations, prevId, id, 'remove', type);
    }
    if (currId && currId !== prevId) {
      this.addOperation(operations, reports[type].id, id, 'create', type);
    }
    return this.resourcesUpdate(operations, 'AssignmentDocument');
  }

  private addOperation(operations: IAssignmentOperation[], docId: number, assignmentId: number, operation: string, opType: string) {
    operations.push({
      document_id: docId,
      assignment_id: assignmentId,
      type: opType,
      operation,
    });
  }

  /**
    * Submits assignment related data
    *
    * @param id
    * @param report Report document
    * @param previous Previous report document
    * @param revenue
  */
  submit(id: number, reports: IReportSet, previous?: IReportSet, revenue?: IRevenue): Promise<any> {
    const operations: IAssignmentOperation[] = [];
    Object.keys(reports).forEach((type) => {
      const prevId = previous[type] && previous[type].id;
      const currId = reports[type].id;
      if (prevId && currId !== prevId) {
        this.addOperation(operations, prevId, id, 'remove', type);
      }
      if (currId && currId !== prevId) {
        this.addOperation(operations, reports[type].id, id, 'create', type);
      }
    });
    return this.resourcesUpdate(operations, 'AssignmentDocument').then(() => {
      if (revenue && revenue.changed) {
        return this.resourcesUpdate([revenue], 'FreelancerRevenue');
      } else {
        return Promise.resolve();
      }
    });
  }


  /**
   * Transforms job data - unifies to job-card data
   *
   * @param job
  */
  private transformJob(job: IJob): any {
    const { count, range, assignments } = this.extract(job);
    const title = job.title.split(' | ')[0];
    return Object.assign(job, { job, title, count, range, assignments }, {
      site: job.site && job.site.data,
      client: job.project && job.project.data.client && job.project.data.client.data || {},
      project: job.project && job.project.data || {},
      start_at: range.dates.start,
      start_time: assignments[0] && assignments[0].start_time,
      finish_time: assignments[0] && assignments[0].finish_time,
    });
  }

  /**
   * Extracts data from job
   *
   * @param job
  */
  private extract(job: IJob): IJobInfo {
    const assignments: IAssignment[] = [];
    const count: IAssignmentsCount = { all: 0, done: 0 };
    const range: IAssignmentsRange = {
      dates: { start: null, end: null },
      rates: { min: 0, max: 0, sum: { min: 0, max: 0 } },
    };
    Collections.pluck(job.dates.data, 'assignments', 'data').forEach((item: IAssignment) => {
      const assignment: IAssignmentDetails = Object.assign(item, {
        start_at: transform.datetime(item.appointed_at, item.start_time),
        end_at: transform.datetime(item.appointed_at, item.finish_time),
        disabled: !item.is_done, templates: '', costs: ''
      });
      count.all++;
      count.done += item.is_done ? 1 : 0;
      if (!range.dates.start || range.dates.start > assignment.start_at) {
        range.dates.start = assignment.start_at;
      }
      if (!range.dates.end || range.dates.end < assignment.end_at) {
        range.dates.end = assignment.end_at;
      }
      const minEstimated = parseFloat(assignment.min_estimated_costs);
      const maxEstimated = parseFloat(assignment.max_estimated_costs);
      if (!range.rates.min || range.rates.min > minEstimated) {
        range.rates.min = minEstimated;
      }
      if (!range.rates.max || range.rates.max < maxEstimated) {
        range.rates.max = maxEstimated;
      }
      range.rates.sum.min += minEstimated;
      range.rates.sum.max += maxEstimated;
      assignments.push(assignment);
    });
    return {
      assignments: assignments.sort(this.byStart),
      count, range
    };
  }


  /**
    * Fetches all freelancer's assignments
    *
    * @param freelancerId
  */
  async assignment(freelancerId: number, assignmentId: number) {
    const opts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json, text/plain, */*'
      }),
      params: new HttpParams()
        .set('include', 'date.job.site,date.job.project.client,documents.approval,revenues,questionnaire,questionnaire_instance,feedback_instance.approval')
    };

    try {
      const data: any = await this.http.get(this.baseUrl + '/freelancers/' + freelancerId + '/assignments/' + assignmentId, opts).toPromise();
      console.log('assignment data', data);

      return Promise.resolve(this.transform(data.data));
    } catch (e) {
      return Promise.reject(e);
    }
  }


  /**
   * Transforms invoice data
   *
   * @param invoice
  */
  private invoiceTransform(invoice: any): any {
    const assignments = Collections.extract(invoice, 'assignments') || [];
    const job: { title: string } = assignments.length && Collections.extract(assignments[0], 'date.job') || { title: '' }; // job from first assignemnt
    const site = Collections.extract(job as any, 'site');
    const approval = Collections.extract(invoice, 'approval');
    job.title = job.title.split(' | ')[0];
    assignments.forEach((item: any) => {
      item.start_at = transform.datetime(item.appointed_at, item.start_time);
      delete item.date.data.job; // to avoid circular structure
    });
    assignments.sort(this.byStart);
    if (invoice.summary) {
      Collections.datify(invoice.summary, ['from', 'to']);
      invoice.summary.from = format.date(invoice.summary.from);
      invoice.summary.to = format.date(invoice.summary.to);
    }
    invoice.issued_at = transform.datetime(invoice.issued_at);
    Collections.stringify(invoice, ['with_discount', 'includes_taxes']);

    return Object.assign(invoice, { assignments, job, site, approval });
  }


  /**
    * Gets single invoice by assignment_id
    *
    * @param freelancerId
    * @param assignmentId
  */
  async getByAssignment(freelancerId: number, assignmentId: number): Promise<any> {
    const opts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json, text/plain, */*'
      }),
      params: new HttpParams()
        .set('include', 'invoices.document,invoices.assignments.incentive_model,invoices.assignments.date.job.site,invoices.creator,invoices.approval')
    };

    try {
      const data: any = await this.http.get(this.baseUrl + '/freelancers/' + freelancerId + '/assignments/' + assignmentId, opts).toPromise();
      console.log('getInvoice data', data);
      const invoices = Collections.extract(data, 'invoices') || [];
      return Promise.resolve(invoices.length && this.invoiceTransform(invoices[0]) || {});
    } catch (e) {
      return Promise.reject(e);
    }
  }

  private byStart(a: any, b: any) {
    if (a.range) {
      return a.range.dates.start - b.range.dates.start;
    } else {
      return a.start - b.start;
    }
  }
}
