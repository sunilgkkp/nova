import { CreateMessagePage } from './../../../app/home/pages/messages/create-message/create-message.page';
import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';

export interface IMsgHeader {
  title?: string;
  info?: string;
  placeholder?: string;
}

export interface IMsgEditable {
  subject?: boolean;
  content?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(
    public modalCtrl: ModalController
  ) { }

  /**
   * Creates message modal
   *
   * @param type Message type (Job currently)
   * @param subject
   * @param content
   * @param additional Additional params to send
   * @param editable Explicitely false to block part of message
  */
  async create(type: string, subject: string, content: string, additional?: any, header?: IMsgHeader, editable?: IMsgEditable): Promise<any> {
    return new Promise<any>(async (resolve, reject) => {
      const message = { subject, content };
      const modal: any = await this.modalCtrl.create({
        component: CreateMessagePage,
        cssClass: 'full',
        componentProps: { type, message, header, editable, data: additional, service: this }
      });
      await modal.present();
      const { data } = modal.onWillDismiss();
      if (data !== undefined) {
        data ? resolve(data) : reject();
      }
    });
  }

  submit(type: string, subject: string, content: string, additional?: any) {
    const data = Object.assign({ subject, content }, additional);
    const method = `create${type}Message`;
    return this[method](data);
  }
}
