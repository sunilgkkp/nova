import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { appConfig } from 'src/controller/appConfig/app.config';

export interface IFilter {
  search?: string;
  [key: string]: any;
}

export interface IApiRequestOptions {
  search?: string;
  params?: any;
  includes?: string[];
  order?: { [key: string]: string };
  headers?: any;
  responseType?: {
    Text
    Json
    ArrayBuffer
    Blob
  };
  paging?: { offset: number, limit: number };
}


@Injectable({
  providedIn: 'root'
})
export class CertificatesService {

  public baseUrl = appConfig.apiBaseUrl;
  constructor(private http: HttpClient) { }

  async list(type: string = 'all', freelancerId: number | string, offset: number, limit: number, filtered: IFilter, page = 1): Promise<any> {
    const opts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json, text/plain, */*'
      }),
      params: new HttpParams()
        .set('without_passed', true)
        .set('order_by', 'is_recommended')
        .set('order_dir', 'desc')
        .set('page', page)
        .set('limit', limit)
    }

    if (filtered && filtered.search) {
      opts.params = opts.params.append('search', filtered.search);
    }

    if (filtered && (filtered.recommendation || []).includes('recommended')) {
      opts.params = opts.params.append('only_recommended', true);
    }

    if (filtered && (filtered.recommendation || []).includes('with_jobs')) {
      opts.params = opts.params.append('with_jobs', true);
    }

    if (filtered && filtered.category) {
      opts.params = opts.params.append('category', filtered.category);
    }

    if (type == 'all') {
      opts.params = opts.params.append('without_exclusive', true);
    }

    if (type === 'exclusive') {
      opts.params = opts.params.append('only_exclusive', true);
    }

    if (type === 'mine') {
      opts.params = undefined;
    }

    try {
      const data = await this.http.get(this.baseUrl + '/freelancers/' + freelancerId + '/certificates', opts).toPromise();
      return Promise.resolve(data);
    } catch (e) {
      return Promise.reject(e);
    }
  }
}
