import { Injectable } from '@angular/core';
import { RestService } from 'src/controller/utility/handler/rest.service';

@Injectable({
  providedIn: 'root'
})
export class NewApiService {

  constructor(
    private restService: RestService,
  ) { }

  getCurrentUser(): any {
    return this.restService.get('/users/current');
  }

}
