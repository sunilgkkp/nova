import { Injectable } from '@angular/core';
import { RequestService } from 'src/controller/utility/handler/request.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { appConfig } from 'src/controller/appConfig/app.config';
import * as moment from 'moment';
import { ApiTransform } from '../api.transform';
import { Format } from 'src/controller/utils/format';

export enum AssignmentState {
  Coming,     // future assignment
  Available,  // checkin available for assignment
  Late,       // late checkin available for assignment
  CheckedIn,  // checkin done
  CheckedOut, // checkout done
  Prepared,   // prepared for invoicing
  Invoiced,   // already invoiced
  Done,       // no more action needed, because assignment is not invoiceable
}


export interface IAssignmentStatus {
  header?: string;
  button?: string;
  state?: AssignmentState;
  class?: {
    today?: boolean;
    available?: boolean;
    delayed?: boolean;
    late?: boolean;
    checkedin?: boolean;
    checkedout?: boolean;
    prepared?: boolean;
    invoiced?: boolean;
    done?: boolean;
  };
}


@Injectable({
  providedIn: 'root'
})
export class AssignmentService {

  public baseUrl = appConfig.apiBaseUrl;
  constructor(
    public request: RequestService,
    private http: HttpClient
  ) { }

  async getAssignments(freelancerId: number, upcoming: boolean = false, include = 'checkins,documents', order_by = 'appointed_at', order_dir = 'asc', offset: number, limit: number, page = 1) {
    const data = { freelancerId, upcoming, include, order_by, order_dir, offset, limit, page }
    return new Promise((resolve, reject) => {
      const success = (value) => {
        if (value) {
          resolve(value);
        }
        else {
          reject(value);
          console.log(value.message);
        }
      };
      this.request.send('getAssignments', data, success);
    });
  }

  /**
     * Gets single freelancer assignment
     *
     * @param freelancerId
     * @param assignmentId
     * @param full True (default) includes full info; base only (checkins) otherwise
     */
  async get(freelancerId: number, assignmentId: number, full: boolean = true): Promise<any> {
    const opts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json, text/plain, */*'
      }),
      params: new HttpParams()
        .set('include', full ? 'certificate,documents,checkins' : 'checkins,documents')
    }

    try {
      const data: any = await this.http.get(this.baseUrl + '/freelancers/' + freelancerId + '/assignments/' + assignmentId, opts).toPromise();
      return Promise.resolve(this.transform(data.data));
    } catch (e) {
      return Promise.reject(e);
    }
  }

  private transform(assignment: any) {
    const start = ApiTransform.datetime(assignment.date.data.appointed_at, assignment.start_time);
    const job = assignment.date.data.job.data;
    const incentives = assignment.incentive_model && this.numbers(assignment.incentive_model);
    const costs = assignment.additional_costs && this.numbers(assignment.additional_costs);
    job.title = job.title.split(' | ')[0];
    return Object.assign(assignment, {
      assignment,
      job,
      incentives,
      costs,
      start_time: assignment.start_time,
      finish_time: assignment.finish_time,
      freelancer: assignment.freelancer && assignment.freelancer.data,
      documents: assignment.documents && assignment.documents.data,
      daily_rate_min: assignment.offer.data.tender.data.daily_rate_min,
      daily_rate_max: assignment.offer.data.tender.data.daily_rate_max,
      offer: assignment.offer.data,
      project: assignment.date.data.job.data.project.data,
      client: assignment.date.data.job.data.project.data.client.data,
      site: assignment.date.data.job.data.site.data,
      start_at: start,
      checkin: {
        available: moment(start).add(appConfig.checkinTolerance.allowed).toDate(),
        late: moment(start).add(appConfig.checkinTolerance.delayed).toDate(),
        done: this.isCheckedIn(assignment),
      },
      checkout: {
        done: this.isCheckedOut(assignment),
      },
      reported: this.isReported(assignment),
      contract_type_identifier: assignment.contract_type && assignment.contract_type.identifier,
      is_prepareable: assignment.is_prepareable,
    });
  }

  private numbers(set: any | any[]): any {
    const res: any = {};
    if (Array.isArray(set)) {
      set.forEach((item: { name: string, value: string }) => res[item.name] = Format.numbers(item.value));
    } else {
      Object.keys(set).forEach((key) => res[key] = Format.numbers(set[key]));
    }
    return res;
  }

  private lastCheckin(assignment: any) {
    const max = assignment.checkins && assignment.checkins.data.length;
    return max && assignment.checkins.data[max - 1];
  }

  private isCheckedIn(assignment: any) {
    return assignment.checkins && assignment.checkins.data.length;
  }

  private isCheckedOut(assignment: any) {
    const last = this.lastCheckin(assignment);
    return last && last.finished_at;
  }

  private isReported(assignment: { documents: { data: any[] } }) {
    return assignment.documents && assignment.documents.data.some((doc: { type: string }) => doc.type === 'report');
  }

  async checkin(assignment: any) {
    const opts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json, text/plain, */*'
      })
    }

    const params = {
      freelancerId: assignment.freelancer.id,
      assignmentId: assignment.id,
    };

    try {
      const data = await this.http.post(this.baseUrl + '/freelancers/' + assignment.freelancer.id + '/assignments/' + assignment.id + '/checkins', params, opts).toPromise();
      return Promise.resolve(data);
    } catch (e) {
      return Promise.reject(e);
    }
  }

  async list(freelancerId: number, upcoming: boolean = false, offset: number, limit: number, page = 1): Promise<any> {
    const opts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json, text/plain, */*'
      }),
      params: new HttpParams()
        .set('only_upcoming', upcoming)
        .set('include', 'checkins,documents')
        .set('order_by', 'appointed_at')
        .set('order_dir', 'asc')
        .set('page', page)
        .set('limit', limit)
    }

    try {
      const data: any = await this.http.get(this.baseUrl + '/freelancers/' + freelancerId + '/assignments', opts).toPromise();
      return Promise.resolve({
        data: data.data.map((item: any) => this.transform(item)),
        meta: data.meta
      });
    } catch (e) {
      return Promise.reject(e);
    }
  }


}
