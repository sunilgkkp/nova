import * as moment from 'moment';
import { apiFormats } from '../appConfig/app.config';


export class ApiPrepare {

    static datetime(datetime?: Date): string {
        return datetime && moment(datetime || new Date()).utc().format(apiFormats.prepare.datetime) || undefined;
    }
}
