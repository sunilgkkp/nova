import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  static readonly NodeUser = 'token';
  userName = '';
  constructor(private storage: Storage) {
    this.storage.create();
    this.storage.get('fName').then(fname => {
      this.userName = fname;
    });
  }

  saveToken(token: string): Promise<any> {
    return this.storage.set(StorageService.NodeUser, token);
  }

  deleteToken() {
    return this.storage.set(StorageService.NodeUser, null);
  }

  getToken() {
    return this.storage.get(StorageService.NodeUser);
  }

  setUserFirstName(fname: string) {
    this.storage.set('fName', fname);
    this.userName = fname
  }

  getUserFirstName() {
    return this.userName;
  }

  set(key, value) {
    this.storage.set(key, value);
  }

  get(key) {
    return this.storage.get(key);
  }

}
