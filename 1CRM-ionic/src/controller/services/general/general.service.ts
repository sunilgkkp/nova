import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  constructor(
    public toastCtrl: ToastController,
    private alertCtrl: AlertController,
    public translate: TranslateService
  ) { }

  async confirmAlert(opts) {
    const buttons = []
      .concat(!opts.persistant &&
        this.button('cancel', opts.cancel || Boolean(opts.onCancel), opts.context, opts.onCancel, opts.buttonCssClass || 'default'))
      .concat(!opts.persistant &&
        this.button('confirm', opts.confirm || Boolean(opts.onConfirm), opts.context, opts.onConfirm, opts.buttonCssClass || 'primary'))
      .concat(opts.persistant &&
        this.button('confirm', 'link', opts.context, opts.onConfirm, opts.buttonCssClass || 'primary'))
      .filter(Boolean);

    const alert = await this.alertCtrl.create({
      header: opts.header && this.text(opts.header, opts.context),
      // subHeader: 'Subtitle',
      message: opts.message && this.text(opts.message, opts.context, opts.item),
      cssClass: 'confirm-alert',
      buttons
    });

    await alert.present();
    const { role } = await alert.onDidDismiss();
  }

  private text(text: string, context: string, item?: any) {
    const isTranslated = /\s/.test(text);
    return (isTranslated && text) || this.translate.instant((context && context + '.' || '') + text, item);
  }

  private label(set: boolean | string, def: string): string {
    return (set === true && def) || String(set);
  }

  private button(role: string, name: string | boolean, context: string, handler: () => void, cssClass: string): any[] {
    return (name && [{
      text: this.translate.instant((context && context + '.' || '') + 'buttons.' + this.label(name, role)),
      role,
      handler,
      cssClass,
    }]) || [];
  }

  async notifyPresent(text: string, duration: number = 2000, item: any = {}): Promise<any> {
    const toast = await this.toastCtrl.create({
      message: this.translate.instant(text, item),
      position: 'bottom',
      duration,
    });
    await toast.present();
  }

  async notifyAction(text: string, button: string, handler: () => void, duration: number = 4000): Promise<any> {
    const toast = await this.toastCtrl.create({
      message: this.translate.instant(text),
      position: 'top',
      duration,
    });
    const { data, role } = await toast.onDidDismiss();
    if (role === 'close') {
      handler();
    }
    return toast.present();
  }

}
