import { RequestService } from 'src/controller/utility/handler/request.service';
import { Injectable } from '@angular/core';
import { IApiUser, UserDataService } from 'src/controller/apiAppData/apiUser/user-data.service';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { appConfig } from 'src/controller/appConfig/app.config';
import { IFilter } from '../certificates/certificates.service';
import { ApiTransform as transform } from '../api.transform';
import { Collections } from 'src/controller/utils/Collections';
import { ApiPrepare as prepare } from '../api.prepare';

export enum JobMatch {
  All,
  Yes,
  No,
}

interface ITendersRange {
  dates: { start: Date, end: Date };
  rates: { min: number, max: number, sum: { min: number, max: number } };
}

interface IMismatched {
  certificates?: string[];
  location?: string[];
  contractType?: string[];
}

interface IJobDetails {
  matching: boolean;
  range: any;
  site: any;
  project: any;
  client: any;
  assignment: any;
  tenders: any[];
  mismatched: IMismatched;
}


@Injectable({
  providedIn: 'root'
})
export class JobService {

  public baseUrl = appConfig.apiBaseUrl;
  constructor(
    private user: UserDataService,
    private request: RequestService,
    private http: HttpClient,
    private translate: TranslateService
  ) { }

  /**
   * Gets single certificate
   *
   * @param trainingId
   */

  /**
   * Gets single certificate
   *
   * @param trainingId
   */
  async getAllCertificates(): Promise<any> {
    try {
      const data = await this.http.get(this.baseUrl + '/certificates').toPromise();
      return Promise.resolve(data);
    } catch (e) {
      return Promise.reject(e);
    }
  }

  /**
  * Transforms contract type
  *
  * @param contract type
  */
  private transformContractTypes(contractType): any {
    contractType.name = contractType.identifier && this.translate.instant('common.contract_types.' + contractType.identifier);
    return contractType;
  }

  // /**
  //    * Gets given job for given freelancer
  //    *
  //    * @param freelancerId
  //    * @param jobId
  //    */
  // async get(freelancerId: number, jobId: number): Promise<any> {
  //   const opts = {
  //     headers: new HttpHeaders({
  //       'Content-Type': 'application/json',
  //       'Accept': 'application/json, text/plain, */*'
  //     }),
  //     params: new HttpParams()
  //       .set('is_matching', true)
  //   }

  //   try {
  //     const data = await this.http.get(this.baseUrl + '/freelancers/' + freelancerId + '/jobs/' + jobId, opts).toPromise();
  //     console.log('data', data);
  //     return Promise.resolve(data);
  //   } catch (e) {
  //     return Promise.reject(e);
  //   }
  // }

  async get(include = 'site,project.client.contacts,contract_type', limit: number) {
    const data = { include, limit }
    return new Promise((resolve, reject) => {
      const success = (value) => {
        if (value) {
          resolve(value);
        }
        else {
          reject(value);
          console.log(value.message);
        }
      };
      this.request.send('jobsAdvertisement', data, success);
    });
  }

  async jobInviteData() {
    return new Promise((resolve, reject) => {
      const success = (value) => {
        if (value) {
          resolve(value);
        }
        else {
          reject(value);
          console.log(value.message);
        }
      };
      this.request.send('jobInvite', {}, success);
    });
  }

  /*
   * Gets all certificates for the filter options
   */
  async getAllContractTypes() {
    const opts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json, text/plain, */*'
      })
    }
    try {
      const data = await this.http.get(this.baseUrl + '/contract-types', opts).toPromise();
      return Promise.resolve(this.transformContractTypes(data));
    } catch (e) {
      return Promise.reject(e);
    }
  }

  /**
     * Gets list of offers for given freelancer
     *
     * @param freelancerId
     * @param filtered
     */
  async offers(selectSegment, freelancerId: number, offset: number, limit: number, filtered: IFilter, page = 1, pages = 1): Promise<any> {
    const opts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json, text/plain, */*'
      }),
      params: new HttpParams()
        .set('include', 'tender.contract_type,freelancer')
        .set('limit', limit)
        .set('page', page)
        .set('order_by', 'expired_at')
        .set('order_dir', 'asc')
        .set('only_fields', 'offers.*,freelancer.id,freelancer.fullname,tender.id,tender.contract_type_identifier,tender.snapshots')
    }
    if (filtered && filtered.search) {
      opts.params = opts.params.append('search', filtered.search);
    }
    if (filtered && filtered.checkbox) {
      filtered.checkbox.forEach((val, key) => {
        opts.params = opts.params.append('status', val);
      });
    }
    if (filtered && filtered.checkbox) {
      opts.params = opts.params.append('page', pages);
    }
    if (filtered && filtered.contractType) {
      opts.params = opts.params.append('contract_type_id', filtered.contractType);
    }
    if (filtered && filtered.only_declined) {
      opts.params = opts.params.append('search', filtered.state && filtered.state.length === 1 && filtered.state[0] === 'declined');
    }
    if (filtered && filtered.with_declined) {
      opts.params = opts.params.append('search', filtered.state && (filtered.state.length === 0 || filtered.state.length === 2));
    }

    try {
      const data: any = await this.http.get(this.baseUrl + '/offers', opts).toPromise();
      return Promise.resolve({
        data: data.data.map((item: any) => this.transform(this.unify(item, 'offered'))).sort(this.byRangeDates),
        meta: data.meta
      });
    } catch (e) {
      return Promise.reject(e);
    }
  }

  private unify(data: any, indicator: string): any {
    data[indicator] = true;
    data.tender.data[indicator] = true;
    return Object.assign(data, {
      title: data?.tender?.data.snapshots.job.title,
      tenders: { data: [data.tender.data] },
      state: indicator === 'offered' && (data.deleted_at ? 'declined' : 'pending'),
      category: data.tender.data.snapshots.job.category,
      contract_type_identifier: data.tender.data.contract_type_identifier,
    });
  }

  private byRangeDates(a: any, b: any) {
    return a.range.dates.start - b.range.dates.start;
  }

  /**
    * Transforms job data
    *
    * @param job
    */
  private transform(job: { title: string, job_name: string, contract_type_identifier: string, tenders: { data: any } }): any {
    const { matching, range, site, assignment, tenders, project, client, mismatched } = this.extract(job.tenders.data);
    // const shortTitle = job?.job_name.split(' | ')[0];
    // const shortTitleOffer = job?.title.split(' | ')[0];
    const contractType = job.contract_type_identifier && this.translate.instant('common.contract_types.' + job.contract_type_identifier);
    return tenders && Object.assign(job, { contractType, range, site, project, assignment, client, tenders, matching, mismatched });
  }

  /**
   * Extracts data from tenders serie
   *
   * @param tenders
   */
  private extract(tenders: any[]): IJobDetails {
    const mismatched: IMismatched = {};
    const range: ITendersRange = {
      dates: { start: null, end: null },
      rates: { min: 0, max: 0, sum: { min: 0, max: 0 } }
    };
    const matching: boolean = tenders && (tenders[0]?.is_matching || tenders[0]?.offered);

    return {
      range,
      matching,
      mismatched,
      site: tenders && tenders[0]?.snapshots.site,
      project: tenders && tenders[0]?.snapshots.project,
      client: tenders && tenders[0]?.snapshots.client,
      assignment: tenders && tenders[0]?.snapshots.assignment,
      tenders: (tenders || [])
        .map((tender) => {
          tender.hasMoreFinancialInfo = (tender.snapshots.incentive_model && tender.snapshots.incentive_model.id) ||
            (tender.snapshots.assignment.additional_costs && tender.snapshots.assignment.additional_costs.length);
          const start = transform.datetime(tender.snapshots.date.appointed_at, tender.snapshots.assignment.start_time);
          const end = transform.datetime(tender.snapshots.date.appointed_at, tender.snapshots.assignment.finish_time);
          // evaluate mismatching
          if (!matching) {
            // mismatched certificates
            if (tender.matching.certificates === false) {
              const certificates = Collections.pluck(Collections.extract(tender, 'matching_details.certificates') || [], 'name');
              mismatched.certificates = Collections.unique((mismatched.certificates || []).concat(certificates));
            }
            // mismatched cities and zip
            if (tender.matching.cities === false || tender.matching.zip === false) {
              mismatched.location = [this.translate.instant('jobs.mismatched.unmatched-location')];
            }
            // mismatched legal certificates
            if (tender.matching.legal === false) {
              const legalCertificate = Collections.extract(tender, 'matching_details.legal');
              mismatched.certificates = Collections.unique((mismatched.certificates || []).concat(legalCertificate.name));
            }

            // mismachted contract type
            if (tender.matching.contract_type === false) {
              const contractTypeIdentifier = Collections.extract(tender, 'contract_type_identifier');
              mismatched.contractType = this.translate.instant('common.contract_types.' + contractTypeIdentifier);
            }
          }
          // set ranges
          if (!range.dates.start || range.dates.start > start) {
            range.dates.start = start;
          }
          if (!range.dates.end || range.dates.end < end) {
            range.dates.end = end;
          }
          if (!range.rates.min || range.rates.min > tender.daily_rate_min) {
            range.rates.min = tender.daily_rate_min;
          }
          if (!range.rates.max || range.rates.max < tender.daily_rate_max) {
            range.rates.max = tender.daily_rate_max;
          }
          range.rates.sum.min += tender.daily_rate_min;
          range.rates.sum.max += tender.daily_rate_max;
          // and extend object
          return Object.assign(tender, { start, end });
        })
        .sort((a: any, b: any) => a.start - b.start),
    };
  }

  /**
    * Gets list of jobs for given freelancer
    *
    * @param freelancerId
    * @param matching Matching flag
    */
  async list(selectSegment, freelancerId: number, matching: JobMatch = JobMatch.Yes, limit: number, filtered: IFilter, page = 1): Promise<any> {
    const opts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json, text/plain, */*'
      }),
      params: new HttpParams()
        .set('include', 'site,project.client.contacts,contract_type')
        .set('limit', limit)
        .set('is_match', true)
        .set('only_recommended', true)
    }

    if (filtered && filtered.search) {
      opts.params = opts.params.append('search', filtered.search);
    }
    if (filtered && filtered?.dates?.start) {
      opts.params = opts.params.append('date_from', prepare.datetime(filtered && filtered.dates && filtered.dates.start));
    }
    if (filtered && filtered?.dates?.end) {
      opts.params = opts.params.append('date_to', prepare.datetime(filtered && filtered.dates && filtered.dates.end));
    }
    if (filtered && filtered?.postcodes?.min) {
      opts.params = opts.params.append('zip_from', filtered && filtered.postcodes && filtered.postcodes.min);
    }
    if (filtered && filtered?.postcodes?.max) {
      opts.params = opts.params.append('zip_to', filtered && filtered.postcodes && filtered.postcodes.max);
    }
    if (filtered && filtered.certificate) {
      opts.params = opts.params.append('certificate_id', filtered && filtered.certificate);
    }
    if (filtered && filtered.contractType) {
      opts.params = opts.params.append('contract_type_id', filtered && filtered.contractType);
    }

    try {
      const data: any = await this.http.get(this.baseUrl + '/job-advertisements', opts).toPromise();
      return Promise.resolve({
        data: data.data.map(this.transform, this).sort(this.byRangeDates),
        meta: data.meta
      });
    } catch (e) {
      return Promise.reject(e);
    }
  }

  async submitOffers(d): Promise<any> {
    const opts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json, text/plain, */*'
      }),
    }

    try {
      const data: any = await this.http.put(this.baseUrl + '/offers', d, opts).toPromise();
      return Promise.resolve(data);
    } catch (e) {
      return Promise.reject(e);
    }
  }

  async rejectTender(freelancer_id, d): Promise<any> {
    const opts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json, text/plain, */*'
      }),
    }

    try {
      const data: any = await this.http.put(this.baseUrl + '/tenders/' + freelancer_id + '/denials', d, opts).toPromise();
      return Promise.resolve(data);
    } catch (e) {
      return Promise.reject(e);
    }
  }

}
