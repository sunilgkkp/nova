import { ActionSheetController, MenuController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GeneralService } from '../general/general.service';
import { AlertController } from '@ionic/angular';
import { NavigationService } from '../navigationRoute/navigation.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { DatePipe } from '@angular/common';


@Injectable({
  providedIn: 'root'
})
export class FunctionService {

  public loading: any;
  constructor(
    private translate: TranslateService,
    public generalService: GeneralService,
    public menuCtrl: MenuController,
    public altCtrl: AlertController,
    public actionSheet: ActionSheetController,
    private iab: InAppBrowser,
    public datePipe: DatePipe,
    public navCtrl: NavigationService
  ) { }

  async presentLoading(text: string) {
    this.translate.instant(text)
  }

  transformDate(date, sequence = 'MMM dd, yyyy') {
    return this.datePipe.transform(date, sequence);
  }

  showError(title: string, message: string): Promise<any> {
    return new Promise((resolve) => {
      const alert: any = this.generalService.confirmAlert({ header: title, message, confirm: true });
    });
  }

  closeMenu() {
    this.menuCtrl.close();
  }

  async presentAlertMessage(message = '') {
    const alert = await this.altCtrl.create({
      cssClass: 'my-custom-class',
      header: 'Warning',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
    const { role } = await alert.onDidDismiss();
  }

  async presentAlertConfirm(message: any, btnText: any) {
    const alert = await this.altCtrl.create({
      cssClass: 'my-custom-class',
      subHeader: 'Success',
      message: message,
      buttons: [
        {
          cssClass: 'secondary',
          role: 'success',
          text: btnText,
          handler: (blah) => {
            this.startSurvey();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah', blah);
          }
        }
      ]
    });
    await alert.present();
  }

  startSurvey() {
    const browser = this.iab.create('https://elevate-staffing.typeform.com/to/zaKfTdZV', '_blank');
    browser.on('loadstop').subscribe(event => {
      console.log('loadstop', event);
      if (event.url === 'https://elevate.rcubinity.com/check-in/') {
        browser.close();
      }
    });
    browser.on('exit').subscribe(event => {
      console.log('exit', event, event.url);
    });
    browser.on('message').subscribe(event => {
      console.log('event => ', event);
      alert("message");
    });
  }


  back(): void {
    this.navCtrl.goBack();
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheet.create({
      header: 'Choose one',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Camera',
        icon: 'camera',
        handler: () => {
          console.log('okay');
        }
      }, {
        text: 'Gallery',
        icon: 'albums',
        handler: () => {
          console.log('Gallery');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
    const { role } = await actionSheet.onDidDismiss();
  }

}
