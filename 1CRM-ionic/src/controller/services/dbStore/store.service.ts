import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor() { }

  store(key, data: any) {
    try {
      if (typeof data === 'string') {
        localStorage.setItem(key, data);
      } else {
        localStorage.setItem(key, JSON.stringify(data));
      }
    } catch (e) {
      console.log(e);
    }
  }

  get(key) {
    try {
     let dataStringify = localStorage.getItem(key); 
     return JSON.parse(dataStringify);
    } catch (e) {
      return localStorage.getItem(key); 
    }
  }
}
