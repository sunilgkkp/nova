import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Device } from '@ionic-native/device/ngx';
import { NotificationEventResponse, Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import { GeneralService } from '../general/general.service';
import { IApiUser, UserDataService } from 'src/controller/apiAppData/apiUser/user-data.service';
import { NewApiService } from 'src/controller/apiAppData/newApi/new-api.service';

export interface IPushHandlers {
  [key: string]: (payload: any) => void;
}
@Injectable({
  providedIn: 'root'
})
export class PushService {

  profile: IApiUser;
  messages: PushObject;
  private handlers: IPushHandlers;
  constructor(
    private storage: Storage,
    private device: Device,
    private user: UserDataService,
    private newApiData: NewApiService,
    private generalService: GeneralService
  ) { }

  isSubscribed(type: any): Promise<boolean> {
    return this.storage.get('notifications').then((settings: string[]) => {
      return settings && settings.includes(type);
    });
  }

  /**
     * Set notification type on/off
     *
     * @param type Notification type
     * @param status Set status
     */
  set(type: string, status: boolean) {
    this.storage.get('notifications').then((settings: string[]) => {
      const set = settings || [];
      if (status) {
        if (!set.includes(type)) {
          set.push(type);
        }
      } else {
        set.splice(set.indexOf(type), 1);
      }
      this.storage.set('notifications', set).then(() => {
        if (set.length) {
          if (!settings) {
            this.registerHandlers();
          } else {
            this.update();
          }
        } else {
          this.unregister();
        }
      });
    });
  }

  update() { }

  unregister() { }

  registerHandlers() {
    this.messages.on('registration').subscribe((res: any) => {
      return this.storage.set('notificationsRegistrationId', res.registrationId).then(() => this.registerAPI());
    });

    this.messages.on('notification').subscribe((notification: NotificationEventResponse) => {
      const type = notification.additionalData.type;
      this.generalService.notifyAction(notification.message, 'push.button.open', () => {
        if (this.handlers[type]) {
          this.handlers[type](Object.assign(notification, { type }));
        }
      });
    });
  }

  registerAPI() {
    return this.storage.get('notifications').then((settings: string[]) => {
      return this.storage.get('notificationsRegistrationId').then((token) => {
        if (this.profile) {
          settings = settings || [];
          if (settings.length) {
            const params = { id: this.profile.id(), device_id: this.device.uuid, device_token: token, notifications: settings };
            return this.newApiData.registerDevice(params);
          } else {
            const params = { id: this.profile.id(), device_id: this.device.uuid };
            return this.newApiData.unregisterDevice(params);
          }
        }
      });
    });
  }

}
