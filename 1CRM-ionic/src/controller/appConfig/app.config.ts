export const appConfig = {
   defaultLang: 'en',
   // apiBaseUrl:'http://3.127.178.10:90',
   apiBaseUrl: 'http://3.68.129.32:90',
   refreshTokenUrl: '/auth/refresh-token',
   checkinTolerance: {
      allowed: { minutes: '' ? parseInt('', 10) : -30 },
      delayed: { minutes: '' ? parseInt('', 10) : 10 },
   },
}
export const appFormats = {
   transform: {
      time: 'HH:mm',
      datetime: 'DD MMM YYYY, HH:mm',
      date: 'DD MMM YYYY',
   },
   prepare: {
      time: 'HH:mm',
      datetime: 'YYYY-MM-DD HH:mm:ss',
      date: 'YYYY-MM-DD',
   },
};


export const appOptions = {
   jobs: {
      offer: {
         states: ['pending', 'declined'],
      }
   },
   bills: {
      invoices: {
         states: ['issued', '', 'payment-authorized', 'money-transfered', 'rejected']
      },
   },
   certificates: {
      categories: ['product', 'brand', 'promotion'],
      recommendation: ['recommended', 'with_jobs']
   },
};

export const apiFormats = {
   transform: appFormats.transform,
   prepare: appFormats.prepare,
};
