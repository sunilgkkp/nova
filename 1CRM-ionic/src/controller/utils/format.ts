import * as moment from 'moment';
import { appFormats } from '../appConfig/app.config';


export class Format {

    static datetime(value: Date | string): string {
        return moment(value).local().format(appFormats.transform.datetime);
    }

    static date(value: Date | string): string {
        return moment(value).local().format(appFormats.transform.date);
    }

    static numbers(value: any): string {
        const val: number = typeof value === 'string' ? Number(value.replace(/,/g, '.').replace(/ /g, '')) : value;
        return val.toFixed(2).replace('.', ',');
    }
}
