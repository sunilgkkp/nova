const Collection: any = {
   pages: [
      { name: 'dashboard', component: 'dashboard', icon: 'calendar', tab: true },
      { name: 'assignments', component: 'assignments', icon: 'calendar', tab: true },
      { name: 'jobposting', component: 'JobsMainPage', icon: 'briefcase', tab: true, notification: ['tenders-matching'] },
      { name: 'invoices', component: 'BillsPage', icon: 'calculator', tab: true },
   ],
   menuData: [
      { name: 'Dashboard', url: '/home/tabs/dashboard', icon: 'apps-outline', slot: 'start' },
      { name: 'To-Do', url: 'home/tabs/to-do', icon: 'list-outline', slot: 'start' },
      { name: 'My Assignments', url: 'home/tabs/assignments', icon: 'calendar-outline', slot: 'start' },
      { name: 'Job Posting', url: 'home/tabs/jobposting', icon: 'briefcase-outline', slot: 'start' },
      { name: 'Job Training', url: 'home/tabs/certificates/exclusive', icon: 'medal-outline', slot: 'start' },
      { name: 'Academy', url: 'home/tabs/certificates', icon: 'school-outline', slot: 'start' },
      { name: 'My Profile', url: 'home/tabs/settings', icon: 'person-outline', slot: 'start' },
      { name: 'Account Settings', url: 'home/tabs/account-settings', icon: 'cog-outline', slot: 'start' }
   ],
   settingPages: {
      profile: 'profile',
      imprint: 'imprint',
      privacy: 'data-privacy',
   },
   languages: [
      { value: 'select', name: 'Select' },
      { value: 'arabic', name: 'Arabic' },
      { value: 'czech', name: 'Czech' },
      { value: 'dutch', name: 'Dutch' },
      { value: 'english', name: 'English' },
      { value: 'german', name: 'German' },
      { value: 'italian', name: 'Italian' },
      { value: 'polish', name: 'Polish' },
      { value: 'spanish', name: 'Spanish' },
      { value: 'turkish', name: 'Turkish' },
      { value: 'other', name: 'Other' },
   ],
   references: [
      { value: '1', name: 'The person managed you directly ' },
      { value: '2', name: 'The person reported you directly ' },
      { value: '3', name: 'The person was senior to you but didn t manage directly ' },
      { value: '4', name: ' You were senior to the person but didnt manage directly ' },
      { value: '5', name: 'You worked with the person in same group ' },
      { value: '6', name: 'The person worked with you but in different group ' },
      { value: '7', name: 'You were a client to the person ' },
      { value: '8', name: 'The person was your client ' },
      { value: '9', name: 'The person mentored you ' },
      { value: '10', name: 'You mentored the person ' },
   ],
   filterCheckbox: [
      { id: 1, value: 'submitted', name: 'Submitted' },
      { id: 2, value: 'rejected', name: 'Rejected' },
      { id: 3, value: 'booked', name: 'Booked' },
      { id: 4, value: 'shortlisted', name: 'Shortlisted' }
   ],
   dlCheckBoxes: [
      { id: 1, value: 'A', name: 'A' },
      { id: 2, value: 'B', name: 'B' },
      { id: 3, value: 'C', name: 'C' },
      { id: 4, value: 'D', name: 'D' },
      { id: 5, value: 'E', name: 'E' },
      { id: 6, value: 'F', name: 'F' }
   ]
}
export default Collection;