
export class Is {

   static array(set: any) {
      return typeof set === 'object' && set.length !== undefined;
   }
}